<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');
Route::get('/journey', 'HomeController@journey');

Route::get('/shop/index', 'ShopController@shop');
Route::post('/register/shop', 'ShopController@registerShop');

Route::get('/basket', 'OrderController@basket');
Route::get('/basket/addProduct', 'OrderController@addProduct');

Route::get('/trainerarea', 'TrainerareaController@index');
Route::get('/trainerarea/shop', 'TrainerareaController@shop');
Route::post('/trainerarea/shop/invite', 'TrainerareaController@shopInvite');
Route::get('/trainerarea/aim', 'TrainerareaController@aim');
Route::post('/trainerarea/aim/create', 'TrainerareaController@aimCreate');
Route::get('/trainerarea/aim/{id}/edit', 'TrainerareaController@aimEdit');
Route::patch('/trainerarea/aim/{id}/update', 'TrainerareaController@aimUpdate');
Route::get('/trainerarea/{id}', 'TrainerareaController@show');
Route::get('/trainerarea/foodplan/check', 'TrainerareaController@foodplanCheck');

// User
Route::get('/usercategorie/{id}/edit', 'UserController@editCategorie');
Route::post('/usercategorie/create', 'UserController@createCategorie');
Route::patch('/usercategorie/{id}', 'UserController@updateCategorie');

Route::get('/user/befriend/{id}', 'UserController@befriend');

Route::get('/trainer/index', 'UserController@trainer');
Route::get('/trainer/{id}/show', 'UserController@trainerShow');

// User -> Userarea
Route::post('/userarea/insta/{access}', 'UserController@instaCreate');
Route::get('/userarea/edit', 'UserController@edit');
Route::post('/userarea/edit/create', 'UserController@addrCreate');
Route::post('/userarea/edit/{id}', 'UserController@addrUpdate');

// Userarea
Route::get('/userarea', 'UserareaController@index');
Route::get('/userarea/friendships', 'UserareaController@friendships');
Route::post('/userarea/journey', 'UserareaController@journey');
Route::get('/userarea/exercise', 'UserareaController@exercise');
Route::get('/userarea/exercise/{id}/edit', 'UserareaController@exerciseEdit');
Route::patch('/userarea/exercise/{id}/update', 'UserareaController@exerciseUpdate');
Route::get('/userarea/trainplan', 'UserareaController@trainplan');
Route::get('/userarea/blog', 'UserareaController@blog');
Route::get('/userarea/recipe', 'UserareaController@recipe');
Route::get('/userarea/foodplan', 'UserareaController@foodplan');
Route::get('/userarea/calc', 'UserareaController@calc');
Route::get('/userarea/calendar', 'UserareaController@calendar');

Route::get('/userarea/aim', 'UserareaController@aim');
Route::post('/userarea/aim/create', 'UserareaController@aimCreate');
Route::patch('/userarea/aim/update', 'UserareaController@aimUpdate');

Route::get('/userarea/pal', 'UserareaController@pal');
Route::post('/userarea/pal/make', 'UserareaController@palMake');
Route::post('/userarea/pal/create', 'UserareaController@palCreate');

// Rezepte
Route::get('/recipe/index', 'RecipeController@index');
Route::get('/recipe/{id}/show', 'RecipeController@show');
Route::get('/recipe/{id}/edit', 'RecipeController@edit');
Route::get('/recipe/{id}/delete', 'RecipeController@delete');
Route::post('/recipe/create', 'RecipeController@create');
Route::patch('/recipe/{recipe}', 'RecipeController@update');
Route::post('/recipe/{id}/add_to_user', 'RecipeController@add_to_user');
Route::post('/recipe/{id}/note', 'RecipeController@note');
Route::post('/recipe/{id}/like', 'RecipeController@like');

Route::get('/recipecategorie/{id}/edit', 'RecipeController@editCategorie');
Route::post('/recipecategorie/create', 'RecipeController@createCategorie');
Route::patch('/recipecategorie/{recipecategorie}', 'RecipeController@updateCategorie');

// Ernährungspläne
Route::get('/foodplan/index', 'FoodplanController@index');
Route::get('/foodplan/{id}/show', 'FoodplanController@show');
Route::get('/foodplan/{id}/edit', 'FoodplanController@edit');
Route::get('/foodplan/{id}/delete', 'FoodplanController@delete');
Route::post('/foodplan/create', 'FoodplanController@create');
Route::patch('/foodplan/{foodplan}', 'FoodplanController@update');
Route::post('/foodplan/{id}/add_to_user', 'FoodplanController@add_to_user');
Route::post('/foodplan/{id}/note', 'FoodplanController@note');
Route::post('/foodplan/{id}/like', 'FoodplanController@like');
Route::get('/foodplan/{id}/personal', 'FoodplanController@personal');

Route::get('/foodplancategorie/{id}/edit', 'FoodplanController@editCategorie');
Route::post('/foodplancategorie/create', 'FoodplanController@createCategorie');
Route::patch('/foodplancategorie/{foodplancategorie}', 'FoodplanController@updateCategorie');

// Träiningpläne
Route::get('/trainplan/index', 'TrainplanController@index');
Route::get('/trainplan/{id}/show', 'TrainplanController@show');
Route::get('/trainplan/{id}/edit', 'TrainplanController@edit');
Route::post('/trainplan/create', 'TrainplanController@create');
Route::patch('/trainplan/{trainplan}', 'TrainplanController@update');
Route::post('/trainplan/{id}/add_to_user', 'TrainplanController@add_to_user');
Route::post('/trainplan/{id}/note', 'TrainplanController@note');
Route::post('/trainplan/{id}/like', 'TrainplanController@like');
Route::get('/trainplan/{id}/personal', 'TrainplanController@personal');

Route::get('/trainplancategorie/{id}/edit', 'TrainplanController@editCategorie');
Route::post('/trainplancategorie/create', 'TrainplanController@createCategorie');
Route::patch('/trainplancategorie/{trainplancategorie}', 'TrainplanController@updateCategorie');

// Übungen
Route::get('/exercise/index', 'ExerciseController@index');
Route::get('/exercise/{id}/show', 'ExerciseController@show');
Route::get('/exercise/{id}/edit', 'ExerciseController@edit');
Route::post('/exercise/create', 'ExerciseController@create');
Route::patch('/exercise/{exercise}', 'ExerciseController@update');
Route::post('/exercise/{id}/add_to_user', 'ExerciseController@add_to_user');
Route::post('/exercise/{id}/note', 'ExerciseController@note');
Route::post('/exercise/{id}/like', 'ExerciseController@like');

Route::get('/exercisecategorie/{id}/edit', 'ExerciseController@editCategorie');
Route::post('/exercisecategorie/create', 'ExerciseController@createCategorie');
Route::patch('/exercisecategorie/{exercisecategorie}', 'ExerciseController@updateCategorie');

// Blogs
Route::get('/blog/index', 'BlogController@index');
Route::get('/blog/{id}/show', 'BlogController@show');
Route::get('/blog/{id}/edit', 'BlogController@edit');
Route::post('/blog/create', 'BlogController@create');
Route::patch('/blog/{blog}', 'BlogController@update');
Route::post('/blog/{id}/add_to_user', 'BlogController@add_to_user');
Route::post('/blog/{id}/note', 'BlogController@note');
Route::post('/blog/{id}/like', 'BlogController@like');

Route::get('/blogcategorie/{id}/edit', 'BlogController@editCategorie');
Route::post('/blogcategorie/create', 'BlogController@createCategorie');
Route::patch('/blogcategorie/{blogcategorie}', 'BlogController@updateCategorie');

// Json requests
Route::get('/gym/exercise/by_categorie', 'ExerciseController@exercises_by_category');
Route::get('/gym/recipe/by_categorie', 'RecipeController@recipes_by_category');
Route::get('/gym/foodplan/by_categorie', 'FoodplanController@foodplans_by_category');
Route::get('/gym/trainplan/by_categorie', 'TrainplanController@trainplans_by_category');

Route::get('/food/find', 'FoodController@find');
Route::get('/food/find/{id}', 'FoodController@find_by_id');

Route::get('/trainer/find', 'UserController@find');
Route::get('/trainer/find/{id}', 'UserController@find_by_id');

Route::get('/foodplan/{id}/day/{name}', 'RecipeController@compareDay');
Route::get('/trainplan/{id}/day/{name}', 'ExerciseController@compareDay');

Route::get('/userarea/events/create', 'UserareaController@eventsCreate');
Route::get('/userarea/events/update', 'UserareaController@eventsUpdate');
Route::get('/userarea/events/get', 'UserareaController@eventsGet');

Route::get('/clndr/events/tpday/{id}', 'ClndrController@tpday');
Route::get('/clndr/events/fpday/{id}', 'ClndrController@fpday');
Route::get('/clndr/events/exercise/{id}', 'ClndrController@exercise');
Route::get('/clndr/events/recipe/{id}', 'ClndrController@recipe');

