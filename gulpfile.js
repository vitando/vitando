const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass([
    	'app.scss',
    	'bootstrap.scss',
    	'magicsuggest.scss',
    	'bootstrap-datepicker.scss',
    	'style.scss'
    	]);
    mix.scripts([
        "jquery.ui.touch-punch.min.js",
        "./node_modules/moment/moment.js",
        "./node_modules/underscore/underscore.js",
        "modal-steps.js",
        "./node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js",
        "magicsuggest.js",
        "clndr.js",
        "./node_modules/chart.js/dist/Chart.js"
   		], 'public/js/addons.js');
    mix.scripts([
        "bootstrap_old.js",
        "forms.js",
        "style.js"
   		], 'public/js/all.js');
});	