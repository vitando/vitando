<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foodplans_categorie extends Model
{
	protected $fillable = ['scope', 'description', 'title'];

	public function foodplans()
	{
		return $this->hasMany(Foodplan::class);
	}
	public function foodplans_categories()
	{
		return $this->belongsTo(Foodplans_Categorie::class, 'scope');
	}
}
