<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use App\Note;
use App\User;
use App\Users_Aim;
use App\Trainplan;
use App\Trainplans_Exercise;
use App\Trainplans_Categorie;
use App\Exercise;
use App\Exercises_Categorie;
use App\Users_Trainplan;
use App\Http\Requests;

class TrainplanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $trainplans_categories = Trainplans_Categorie::all();
        $trainplans = Trainplan::all();

        return view('trainplan.index', compact('trainplans_categories','trainplans'));
    }

    public function show($id)
    {
        $trainplans = Trainplan::all();
        $trainplan = Trainplan::where('id', $id)->first();
        $notes = Note::where('note_type', 'trainplan')->where('note_id', $id)->get();


        if(User::find(Auth::id())->trainplans()->where('trainplan_id', $id)->first()){
            $check = true;
        }else{
            $check = false;
        }

        return view('trainplan.show', compact('trainplans', 'trainplan', 'check', 'notes'));
    }

    // Funktionen für Trainingsplan

    public function create(Request $request)
    {
    	$i = 0;
        $duration = 0;
    	$trainplan = new Trainplan;

        $trainplan->user_id = Auth::id();
        $trainplan->aim_id = $request->aim_id;
        $trainplan->title = $request->title;
        $trainplan->trainplans_categorie_id = $request->trainplans_categorie_id;
        $trainplan->description = $request->description;
        $trainplan->duration = $duration;
        $trainplan->price = $request->price;
        $trainplan->type = $request->type;

        $trainplan->save(); 

        foreach ($request->exercises as $exercise) {
        	if ($i == 0) {
        		$trainplans_exercises = new Trainplans_Exercise;
                $trainplans_exercises->day = $exercise;
        		$i++;
        	}
        	else if ($i == 1) {
        		$trainplans_exercises->exercise_id = $exercise;
        		$i++;
        	}
        	else if ($i == 2) {
        		$trainplans_exercises->sets = $exercise;
        		$i++;
        	}
        	else if ($i == 3) {
        		$trainplans_exercises->reps = $exercise;
        		$i++;
        	}
        	else if ($i == 4) {
        		$trainplans_exercises->rm_value = $exercise;
        		$i++;
        	}
        	else if ($i == 5) {
        		$trainplans_exercises->description = $exercise;
        		$trainplans_exercises->trainplan_id = $trainplan->id;

        		$trainplans_exercises->save();
        		$duration++;
        		$i = 0;
        	}
        }

        $updateDuration = Trainplan::find($trainplan->id);
        $updateDuration->duration = $duration;
        $updateDuration->save();

        return back();
    }

    public function edit($id)
    {
        $trainplans_categories = Trainplans_Categorie::all();
        $exercises_categories = Exercises_Categorie::all();
        $exercises = Exercise::all();
        $trainplans = Trainplan::all();
        $trainplan = Trainplan::where('id', $id)->first();


        return view('trainplan.edit', compact('trainplans_categories','exercises_categories','exercises','trainplans','trainplan'));
    }

    public function update(Request $request, Trainplan $trainplan)
    {
        $i = 0;
        $trainplan->update(['title' => $request->title, 'description' => $request->description, 'trainplans_categorie_id' => $request->trainplans_categorie_id]);

        foreach ($request->exercises as $exercise) {
            if ($i == 0 && $trainplan->trainplans_exercises()->find($exercise)) {
                $exercise_id = $trainplan->trainplans_exercises()->find($exercise);
                $status = "update";
                $i++;
            }
            else if ($i == 0 && $exercise = 'new') {
                $exercise_id = new Trainplans_Exercise;
                $status = "new";
                $i++;
            }
            else if ( $i == 1) {
                if ($exercise == 'delete') {
                    $status = "delete";
                }
                $exercise_id->day = $exercise;
                $i++;
            }
            else if ($i == 2) {
                $exercise_id->exercise_id = $exercise;
                $i++;
            }
            else if ($i == 3) {
                $exercise_id->sets = $exercise;
                $i++;
            }
            else if ($i == 4) {
                $exercise_id->reps = $exercise;
                $i++;
            }
            else if ($i == 5) {
                $exercise_id->rm_value = $exercise;
                $i++;
            }
            else if ($i == 6) {
                $exercise_id->description = $exercise;
                $exercise_id->trainplan_id = $trainplan->id;
                if ($status == "new") {
                    $exercise_id->save();
                    $i = 0;
                }
                else if ($status == "update") {
                    $exercise_id->update();
                    $i = 0;
                }
                else if ($status == "delete") {
                    $exercise_id->delete();
                    $i = 0;
                }

            }
        }

        return back();
    }

    //  Funktionen für die Kategorien von Trainingsplänen

    public function createCategorie(Request $request)
    {
        $trainplans_categorie = new Trainplans_Categorie;

        $trainplans_categorie->title = $request->title;
        $trainplans_categorie->scope = $request->scope;
        $trainplans_categorie->description = $request->description;

        $trainplans_categorie->save(); 
        
        return back();
    }

    public function editCategorie($id)
    {
        
        $trainplans_categories = Trainplans_Categorie::all();
        $trainplans_categorie = Trainplans_Categorie::where('id', $id)->first();

        return view('trainplan.edit_categorie', compact('trainplans_categories', 'trainplans_categorie'));
    }

    public function updateCategorie(Request $request, Trainplans_Categorie $trainplancategorie)
    {
        $trainplancategorie->update($request->all());

        return back();
    }

    public function add_to_user($id)
    {
        $trainplan = new Users_Trainplan;

        $trainplan->user_id = Auth::id();
        $trainplan->trainplan_id = $id;

        $trainplan->save(); 
        
        return back();
    }

    public function note($id, Request $request)
    {
        $note = new Note;

        $note->user_id = Auth::id();

        $note->note_type = 'trainplan';
        $note->note_id = $id;
        $note->value = $request->note;
        $note->save();
        
        return back();
    }

    public function personal($id)
    {
        $trainplan = Trainplan::find($id);
        $trainplans_exercises = $trainplan->trainplans_exercises()->orderBy('day', 'asc');
        $day_old = 'false';

        foreach ($trainplans_exercises->get() as $key => $exercise) {
            if ($exercise->day == $day_old || $key == 0) {
                $day_old = $exercise->day;
                if ($key == 0) {
                    $title = $exercise->exercise->title;
                }else{
                    $title = $title . ', ' . $exercise->exercise->title;
                }
            }else{
                $days[$day_old] = ['title' => $title];
                $day_old = $exercise->day;
                $title = NULL;
                $title = $exercise->exercise->title;
            }
        }
        $days[$day_old] = ['title' => $title];

        $aim = Users_aim::where('user_id', Auth::id())->first();

        return view('trainplan.user_create', compact('trainplan','trainplans_exercises','days','aim'));
    }

    // jquery json requests

    public function Trainplans_by_category(Request $request)
    {
        $trainplan_categorie_id = $request->categorie_id;
        $exercises = DB::table('trainplans')->select('id', 'title')->where('trainplans_categorie_id', '=', $exercise_categorie_id)->get();

        return $exercises->toJson();
    }
}
