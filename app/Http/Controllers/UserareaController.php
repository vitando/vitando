<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;
use DateTime;
use App\Aim;
use App\User;
use App\Users_Aim;
use App\Users_Cat;
use App\Users_Event;
use App\Users_Exercise;
use App\Users_Trainplan;
use App\Users_Foodplan;
use App\Users_Blog;
use App\Users_Recipe;
use App\Users_Pal;
use App\Traits\general;

class UserareaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Users_Aim $users_aim)
    {   
        $aim = $users_aim->where('user_id', Auth::id())->first();
        if ($aim) {
            return view('userarea.index', compact('aim'));
        }
        return view('userarea.index');
    }

    public function friendships()
    {   
        $user = User::find(Auth::id());
        $befriends = $user->getPendingFriendships();
        $aim = Users_Aim::where('user_id', Auth::id())->first();

        foreach ($befriends as $key => $user) {
            $newfriends = User::find($user->sender_id);
            $befriends[$key]['nick'] = $newfriends->name;
        }

        return view('userarea.friendship', compact('user', 'befriends', 'aim'));
    }

    public function journey(Request $request)
    {   
        $aim = new Users_Aim;
        $aim->user_id = Auth::id();
        $aim->aim_id = $request->aim_id;
        $aim->weight = $request->aim_weight;
        $aim->note = $request->aim_note;
        $aim->save();

        $user = User::find(Auth::id());
        $user->weight = $request->weight;
        $user->bday = $request->bday;
        $user->size = $request->size;
        $user->gender = $request->gender;
        $user->save();

        return view('userarea.index');
    }

    public function aim()
    {
        $aims = Aim::all();
        return view('userarea.aim', compact('aims'));
    }
    
    public function aimCreate(Request $request)
    {
        $aim = new Users_Aim;
        $aim->user_id = Auth::id();
        $aim->aim_id = $request->aim_id;
        $aim->weight = $request->weight;
        $aim->note = $request->note;
        $aim->save();
        $url = action('UserareaController@index');
        return redirect($url);
    }

    public function aimUpdate(Users_Aim $users_aim, Request $request)
    {
        $aim = $users_aim->where('user_id', Auth::id())->first();
        $aim->aim_id = $request->aim_id;
        $aim->weight = $request->weight;
        $aim->note = $request->note;
        $aim->save();
        return back();
    }

    public function calc(Users_Pal $pal)
    {
        $aim = Users_Aim::where('user_id', Auth::id())->first();
        $aims = Aim::all();
        $user_pals = $pal->where('user_id', Auth::id())->get();
        foreach ($user_pals as $key => $pal) {
            $general = new general;
            $energy_aim[$pal->id] = ($general->calcAim($pal->energy_total));
        }
        return view('userarea.calc', compact('user_pals', 'energy_aim', 'aim', 'aims'));
    }

    public function calendar()
    {
        $aim = Users_Aim::where('user_id', Auth::id())->first();
        return view('userarea.calendar', compact('aim'));
    }

    public function eventsCreate(Request $request)
    {
        $events = json_decode($request->data);
        $user_id = Auth::id();
        foreach ($events as $value) {
            $event = new Users_Event;
            $event->user_id = $user_id;
            $event->event_type = $value->event->event_type;
            $event->event_id = $value->event->event_id;
            $event->event_name = $value->event->event_name;
            $event->event_date = $value->event->event_date;
            $event->save();
        }
    }
    public function eventsUpdate(Request $request)
    {
        $events = json_decode($request->data);
        $user_id = Auth::id();

        foreach ($events as $value) {
            $event = Users_Event::find($value->event->data_id);
            if($event->user_id == $user_id) {
                if($value->event->event_type == 'delete') {
                    $event->delete();
                }else{
                    $event->event_date = $value->event->event_date;
                    $event->save();
                }
            }
        }
    }
    // JSON Request von User-Events für clndr.js
    public function eventsGet()
    {
        $events = Users_Event::where('user_id', Auth::id())->get();
        return $events->toJson();
    }
    
    // Öffnet favourisierte Übungen in Userarea
    public function exercise(Users_Exercise $exercises)
    {
        $user_exercises = $exercises->where('user_id', Auth::id())->get();
        $aim = Users_Aim::where('user_id', Auth::id())->first();

        return view('userarea.exercise', compact('user_exercises', 'aim'));
    }
    // Öffnet Bearbeitung von favourisierter Übungen aus Userarea
    public function exerciseEdit($id)
    {
        $user_exercise = Users_Exercise::find($id);
        $history = $user_exercise->revisionHistory;
        $aim = Users_Aim::where('user_id', Auth::id())->first();

        return view('exercise.user_edit', compact('user_exercise', 'history', 'aim'));
    }
    // Speichert Bearbeitung von favourisierter Übungen aus Userarea
    public function exerciseUpdate($id, Request $request)
    {
        $user_exercise = Users_Exercise::find($id);
        $user_exercise->rm_value = $request->rm_value;
        $user_exercise->train_value = $request->train_value;
        $user_exercise->save();

        return back();
    }

    // Öffnet favourisierte Blogs in Userarea
	public function blog(Users_Blog $blog)
    {
        $user_blogs = $blog->where('user_id', Auth::id())->get();
        $aim = Users_Aim::where('user_id', Auth::id())->first();

    	return view('userarea.blog', compact('user_blogs','aim'));
    }

    // Öffnet favourisierte Trnäningspläne in Userarea
    public function trainplan(Users_Trainplan $user_trainplans, Users_Aim $users_aim)
    {
        $user_trainplans = $user_trainplans->where('user_id', Auth::id())->get();
        $aim = $users_aim->where('user_id', Auth::id())->first();
        if ($aim) {
            return view('userarea.trainplan', compact('aim', 'user_trainplans'));
        }else{
            return view('userarea.trainplan', compact('user_trainplans'));
        }

        return view('userarea.trainplan', compact('user_trainplans'));
    }

    // Öffnet favourisierte Ernährungspläne in Userarea
    public function foodplan(Users_Foodplan $user_foodplans, Users_Aim $users_aim)
    {
        $user_foodplans = $user_foodplans->where('user_id', Auth::id())->get();
        $aim = $users_aim->where('user_id', Auth::id())->first();
        if ($aim) {
            return view('userarea.foodplan', compact('aim', 'user_foodplans'));
        }else{
            return view('userarea.foodplan', compact('user_foodplans'));
        }
    }

    // Öffnet favourisierte Rezepte in Userarea
    public function recipe(Users_Recipe $user_recipes, Users_Aim $users_aim)
    {
        $user_recipes = $user_recipes->where('user_id', Auth::id())->get();
        $aim = $users_aim->where('user_id', Auth::id())->first();
        return view('userarea.recipe', compact('user_recipes','aim'));
    }

    // Öffnet User PAL Seite
    public function pal()
    {
        $user = User::find(Auth::id());
        $aim = Users_Aim::where('user_id', Auth::id())->first();
        //date in mm/dd/yyyy format; or it can be in other formats as well
        $birthDate = $user->bday;
        if (!empty($birthDate)) {
            $birthday = new DateTime($birthDate);
        }else{
            $birthday = new DateTime();
        }
        $interval = $birthday->diff(new DateTime);
        $user->age = $interval->y;

        return view('userarea.pal', compact('user', 'aim'));
    }

    // Berechnet PAL aus Eingabe von PAL Seite und öffnet PAL Seite
    public function palMake(Request $request)
    {
        $gender = $request->gender;
        $weight = $request->weight;
        $size = $request->size;
        $age = $request->age;

        if ($gender == 0) {
            $energy = 66.47 + (13.7 * $weight) + (5 * $size) - (6.8 * $age);
        }else{
            $energy = 655.1 + (9.6 * $weight) + (1.8 * $size) - (4.7 * $age);
        }

        $i = 0;
        $time = 0;
        $pal_day = 0;
        foreach ($request->pal as $key => $value) {
            if ($i == 0) {
                $time = $value;
                $i++;
            }else{
                $pal_day = $pal_day + $time * $value;
                $i = 0;
            }
        }

        $pal_day = $pal_day / 24;
        $energy_total = $pal_day * $energy;

        $energy = substr($energy, 0, 7);
        $pal_day = substr($pal_day, 0, 4);
        $energy_total = substr($energy_total, 0, 7);

        $user = User::find(Auth::id());
        $birthDate = $user->bday;
        if (!empty($birthDate)) {
            $birthday = new DateTime($birthDate);
        }else{
            $birthday = new DateTime();
        }
        $interval = $birthday->diff(new DateTime);
        $user->age = $interval->y;

        $aim = Users_Aim::where('user_id', Auth::id())->first();

        return view('userarea.pal', compact('user','aim','energy','pal_day','energy_total'));
    }

    // Speichert berechnete Werte aus PAL Seite 
    public function palCreate(Request $request)
    {
        $pal = new Users_Pal;
        $pal->user_id = Auth::id();
        $pal->title = $request->title;
        $pal->energy = $request->energy;
        $pal->pal_day = $request->pal_day;
        $pal->energy_total = $request->energy_total;
        $pal->save();

        return redirect('/userarea/calc');
    }

}
