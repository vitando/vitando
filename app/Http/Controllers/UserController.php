<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;
use App\User;
use App\Users_Cat;
use App\Users_Addr;
use App\Users_Aim;
use App\Users_Categorie;

class UserController extends Controller
{
    public function edit()
    {   
        $user = User::where('id', Auth::id())->first();
        $user_addr = Users_Addr::where('user_id', Auth::id())->where('type', 'default')->first();
        $aim = Users_Aim::where('user_id', Auth::id())->first();
        if ($user->type == 'trainer') {
            $cats = Users_Categorie::all();
        }
        return view('userarea.edit', compact('user', 'user_addr', 'aim', 'cats'));
    }

    public function addrCreate(Request $request)
    {   
        $user_addr = new Users_Addr;
        $user_addr->user_id = Auth::id();
        $user_addr->type = $request->type;
        $user_addr->email = $request->email;
        $user_addr->title= $request->title;
        $user_addr->vname = $request->vname;
        $user_addr->nname = $request->nname;
        $user_addr->street = $request->street;
        $user_addr->street_number = $request->street_number;
        $user_addr->street_add = $request->street_add;
        $user_addr->zip_code = $request->zip_code;
        $user_addr->town = $request->town;
        $user_addr->save();

        return back();
    }
    
    public function addrUpdate($id, Request $request)
    {   
        $user = User::find(Auth::id());
        $user_addr = Users_Addr::find($id);

        if ($request->type == 'default') {
            $user->weight = $request->weight;
            $user->size = $request->size;
            $user->bday = $request->bday;
            $user->save();
        }else{
            $user_addr->type = $request->type;
        }
        $user_addr->email = $request->email;
        $user_addr->title = $request->title;
        $user_addr->vname = $request->vname;
        $user_addr->nname = $request->nname;
        $user_addr->street = $request->street;
        $user_addr->street_number = $request->street_number;
        $user_addr->street_add = $request->street_add;
        $user_addr->zip_code = $request->zip_code;
        $user_addr->town = $request->town;
        $user_addr->save();

        return back();
    }

    public function instaCreate($access)
    {   
        $instatoken = User::find(Auth::id());
        $instatoken->instatoken = $access;
        $instatoken->save();
    }

    //  Funktionen für die Kategorien von Übungen

    public function createCategorie(Request $request)
    {
        $users_categorie = new Users_Categorie;

        $users_categorie->title = $request->title;
        $users_categorie->scope = $request->scope;
        $users_categorie->description = $request->description;

        $users_categorie->save(); 
        
        return back();
    }

    public function befriend($id)
    {
        $user = User::find(Auth::id());
        $recipient = User::find($id);
        $user->befriend($recipient);

        return back();
    }

    public function editCategorie($id)
    {
        $users_categories = Users_Categorie::all();
        $users_categorie = Users_Categorie::where('id', $id)->first();

        return view('user.edit_categorie', compact('users_categories', 'users_categorie'));
    }

    public function updateCategorie(Request $request, Users_Categorie $id)
    {
        $id->update($request->all());

        return back();
    }

    public function find()
    {
        $trainer = User::all(['id', 'name']);
        return $trainer->toJson();
    }
    
    public function find_by_id($id)
    {
        $trainer = User::all(['id', 'name'])->where('id', $id);
        
        return $trainer->toJson();

    }

    public function trainerShow($id)
    {   
        $trainer = User::find($id);
        $cats = Users_Cat::all()->where('user_id', $id);
        return view('user.trainerShow', compact('trainer', 'cats'));
    }

    public function trainer()
    {   
        $trainers = User::all()->where('type', 'trainer');
        return view('user.trainer', compact('trainers'));
    }


}
