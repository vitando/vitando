<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use App\Note;
use App\User;
use App\Users_Aim;
use App\Users_Pal;
use App\Foodplan;
use App\Foodplans_Recipe;
use App\Foodplans_Categorie;
use App\Recipe;
use App\Recipes_Categorie;
use App\Users_Foodplan;
use App\Traits\general;

use App\Http\Requests;

class FoodplanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $foodplans_categories = Foodplans_Categorie::all();
        $foodplans = Foodplan::all();

        return view('foodplan.index', compact('foodplans_categories','foodplans'));
    }

    public function show($id)
    {
        $foodplans = Foodplan::all();
        $foodplan = Foodplan::where('id', $id)->first();
        $notes = Note::where('note_type', 'foodplan')->where('note_id', $id)->get();


        if(User::find(Auth::id())->foodplans()->where('foodplan_id', $id)->first()){
            $check = true;
        }else{
            $check = false;
        }

        return view('foodplan.show', compact('foodplans', 'foodplan', 'check', 'notes'));
    }

    // Funktionen für Trainingsplan

    public function create(Request $request)
    {
    	$i = 0;
        $duration = 0;
    	$foodplan = new Foodplan;

        $foodplan->user_id = Auth::id();
        $foodplan->aim_id = $request->aim_id;
        $foodplan->title = $request->title;
        $foodplan->type = 'default';
        $foodplan->foodplans_categorie_id = $request->foodplans_categorie_id;
        $foodplan->description = $request->description;
        $foodplan->duration = $duration;
        $foodplan->price = $request->price;

        $foodplan->save(); 

        foreach ($request->recipes as $recipe) {
        	if ($i == 0) {
        		$foodplans_recipes = new Foodplans_Recipe;
                $foodplans_recipes->day = $recipe;
        		$i++;
        	}
        	else if ($i == 1) {
        		$foodplans_recipes->recipe_id = $recipe;
        		$i++;
        	}
        	else if ($i == 2) {
        		$foodplans_recipes->amount = $recipe;
        		$i++;
        	}
        	else if ($i == 3) {
        		$foodplans_recipes->description = $recipe;
        		$foodplans_recipes->foodplan_id = $foodplan->id;

        		$foodplans_recipes->save();
        		$duration++;
        		$i = 0;
        	}
        }

        $updateDuration = Foodplan::find($foodplan->id);
        $updateDuration->duration = $duration;
        $updateDuration->save();

        return back();
    }

    public function edit($id)
    {
        $foodplans_categories = Foodplans_Categorie::all();
        $recipes_categories = Recipes_Categorie::all();
        $recipes = Recipe::all();
        $foodplans = Foodplan::all();
        $foodplan = Foodplan::where('id', $id)->first();


        return view('foodplan.edit', compact('foodplans_categories','recipes_categories','recipes','foodplans','foodplan'));
    }
    public function delete($id)
    {
        $foodplan = Foodplan::find($id);
        if ($foodplan->user_id == Auth::id()) {
            $foodplan->delete();
        }
        foreach ($foodplan->foodplans_recipes as $key => $recipes) {
            $recipes->delete();
        }
        return back();
    }

    public function update(Request $request, Foodplan $foodplan)
    {
        $i = 0;
        $foodplan->update(['title' => $request->title, 'description' => $request->description, 'foodplans_categorie_id' => $request->foodplans_categorie_id]);

        foreach ($request->recipes as $recipe) {
            if ($i == 0 && $foodplan->foodplans_recipes()->find($recipe)) {
                $recipe_id = $foodplan->foodplans_recipes()->find($recipe);
                $status = "update";
                $i++;
            }
            else if ($i == 0 && $recipe = 'new') {
                $recipe_id = new Foodplans_Recipe;
                $status = "new";
                $i++;
            }
            else if ( $i == 1) {
                if ($recipe == 'delete') {
                    $status = "delete";
                }
                $recipe_id->day = $recipe;
                $i++;
            }
            else if ($i == 2) {
                $recipe_id->recipe_id = $recipe;
                $i++;
            }
            else if ($i == 3) {
                $recipe_id->amount = $recipe;
                $i++;
            }
            else if ($i == 4) {
                $recipe_id->description = $recipe;
                $recipe_id->foodplan_id = $foodplan->id;
                if ($status == "new") {
                    $recipe_id->save();
                    $i = 0;
                }
                else if ($status == "update") {
                    $recipe_id->update();
                    $i = 0;
                }
                else if ($status == "delete") {
                    $recipe_id->delete();
                    $i = 0;
                }

            }
        }

        return back();
    }

    //  Funktionen für die Kategorien von Übungen

    public function createCategorie(Request $request)
    {
        $foodplans_categorie = new Foodplans_Categorie;

        $foodplans_categorie->title = $request->title;
        $foodplans_categorie->scope = $request->scope;
        $foodplans_categorie->description = $request->description;

        $foodplans_categorie->save(); 
        
        return back();
    }

    public function editCategorie($id)
    {
        
        $foodplans_categories = Foodplans_Categorie::all();
        $foodplans_categorie = Foodplans_Categorie::where('id', $id)->first();

        return view('foodplan.edit_categorie', compact('foodplans_categories', 'foodplans_categorie'));
    }

    public function updateCategorie(Request $request, Foodplans_Categorie $foodplancategorie)
    {
        $foodplancategorie->update($request->all());

        return back();
    }

    public function add_to_user($id)
    {
        $foodplan = new Users_Foodplan;

        $foodplan->user_id = Auth::id();
        $foodplan->foodplan_id = $id;

        $foodplan->save(); 
        
        return back();
    }

    public function note($id, Request $request)
    {
        $note = new Note;

        $note->user_id = Auth::id();

        $note->note_type = 'foodplan';
        $note->note_id = $id;
        $note->value = $request->note;
        $note->save();
        
        return back();
    }

    public function personal($id, Users_Pal $pal)
    {
        $user_pals = $pal->where('user_id', Auth::id())->get();
        foreach ($user_pals as $key => $pal) {
            $general = new general;
            $users_aim[$pal->id] = ($general->calcAim($pal->energy_total));
        }

        // Ernährungsplan laden und Deklarationsteil
        $foodplan = Foodplan::find($id);
        $foodplans_recipes  = $foodplan->foodplans_recipes()->orderBy('day', 'asc');
        $day_old = 'false';
        $title = '';
        $kcal = 0;
        $fat = 0;
        $carbs = 0;
        $protein = 0; 
        $day = 0; 
        // Array für Tage erstellen mit Berechnung von Nährwerten anhand von Rezepten dessen Menge
        foreach ($foodplans_recipes->get() as $key => $recipe) {
            if ($recipe->day == $day_old || $key == 0) {
                $day_old = $recipe->day;
                if ($key == 0) {
                    $title = $recipe->recipe->title;
                }else{
                    $title = $title . ', ' . $recipe->recipe->title;
                }

                $percent = (1 / $recipe->recipe->amount) * $recipe->amount;
                $kcal = $kcal + ($recipe->recipe->energy_kcal * $percent);
                
                $fat = $fat + ($recipe->recipe->fat * $percent);
                $carbs = $carbs + ($recipe->recipe->carbs * $percent);
                $protein = $protein + ($recipe->recipe->protein * $percent);

            }else{
                $days[$day_old] = ['kcal' => $kcal, 'fat' => $fat, 'carbs' => $carbs, 'protein' => $protein, 'title' => $title];
                $day_old = $recipe->day;
                
                $title = NULL;
                $kcal = NULL;
                $fat = NULL;
                $carbs = NULL;
                $protein = NULL;

                $percent = (1 / $recipe->recipe->amount) * $recipe->amount;
                $title = $recipe->recipe->title;
                $kcal = $kcal + ($recipe->recipe->energy_kcal * $percent);
                $fat = $fat + ($recipe->recipe->fat * $percent);
                $carbs = $carbs + ($recipe->recipe->carbs * $percent);
                $protein = $protein + ($recipe->recipe->protein * $percent);
                
            }
        }
        $days[$day_old] = ['kcal' => $kcal, 'fat' => $fat, 'carbs' => $carbs, 'protein' => $protein, 'title' => $title];
        $general = new general;
        $compare =  ($general->compareDay($days));

        $aim = Users_Aim::where('user_id', Auth::id())->first();

        return view('foodplan.user_create', compact('foodplan','foodplans_recipes','days','compare','user_pals','users_aim','aim'));
    }

    // jquery json requests

    public function foodplans_by_category(Request $request)
    {
        $foodplan_categorie_id = $request->categorie_id;
        $recipes = DB::table('foodplans')->select('id', 'title')->where('foodplans_categorie_id', '=', $recipe_categorie_id)->get();

        return $recipes->toJson();
    }

}
