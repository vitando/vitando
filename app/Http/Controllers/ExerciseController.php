<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Response;
use App\User;
use App\Note;
use App\Like;
use App\Exercise;
use App\Users_Exercise;
use App\Exercises_Categorie;
use App\Trainplans_Exercise;
use App\Http\Requests;

class ExerciseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $exercises_categories = Exercises_Categorie::all();
        $exercises = Exercise::all();

        return view('exercise.index', compact('exercises_categories','exercises'));
    }

    public function show($id)
    {
        $exercises = Exercise::all();
        $exercise = Exercise::where('id', $id)->first();
        $notes = Note::where('note_type', 'exercise')->where('note_id', $id)->get();


        if(Like::all()
            ->where('user_id', Auth::id())
            ->where('like_type', 'exercise')
            ->where('like_id', $id)
            ->where('value', 'true')
            ->first())
        {
            $like = true;
        }
        else
        {
            $like = false;
        }

        if(User::find(Auth::id())->exercises()->where('exercise_id', $id)->first()){
            $check = true;
        }
        else
        {
            $check = false;
        }

        return view('exercise.show', compact('exercise', 'exercises', 'check', 'notes', 'like'));
    }

    public function add_to_user($id)
    {
        $exercise = new Users_Exercise;

        $exercise->user_id = Auth::id();
        $exercise->exercise_id = $id;

        $exercise->save(); 
        
        return back();
    }

    public function note($id, Request $request)
    {
        $note = new Note;

        $note->user_id = Auth::id();

        $note->note_type = 'exercise';
        $note->note_id = $id;
        $note->value = $request->note;
        $note->save();
        
        return back();
    }

    public function like($id)
    {
        if(Like::all()
            ->where('user_id', Auth::id())
            ->where('like_type', 'exercise')
            ->where('like_id', $id)
            ->where('value', 'true')
            ->first())
        {
            $like = Like::all()
            ->where('user_id', Auth::id())
            ->where('like_type', 'exercise')
            ->where('like_id', $id)
            ->first();

            $like->value = 'false';

        }else if(Like::all()
            ->where('user_id', Auth::id())
            ->where('like_type', 'exercise')
            ->where('like_id', $id)
            ->where('value', 'false')
            ->first())
        {
            $like = Like::all()
            ->where('user_id', Auth::id())
            ->where('like_type', 'exercise')
            ->where('like_id', $id)
            ->first();

            $like->value = 'true';
        }
        else{
            $like = new Like;
            $like->user_id = Auth::id();
            $like->like_type = 'exercise';
            $like->like_id = $id;
            $like->value = 'true';
        }

        $like->save();
        
        return back();
    }

    public function create(Request $request)
    {
        $exercise = new Exercise;

        $exercise->user_id = Auth::id();
        $exercise->title = $request->title;
        $exercise->exercises_categorie_id = $request->category;
        $exercise->description = $request->description;

        $exercise->save(); 
        
        return back();
    }
    public function edit($id)
    {
        $exercises_categories = Exercises_Categorie::all();
        $exercises = Exercise::all();
        $exercise = Exercise::where('id', $id)->first();

        return view('exercise.edit', compact('exercises_categories','exercises','exercise'));
    }

    public function update(Request $request, Exercise $exercise)
    {
        $exercise->update($request->all());

        return back();
    }

    public function createCategorie(Request $request)
    {
        $exercises_categorie = new Exercises_Categorie;

        $exercises_categorie->title = $request->title;
        $exercises_categorie->scope = $request->scope;
        $exercises_categorie->description = $request->description;

        $exercises_categorie->save(); 
        
        return back();
    }

    public function editCategorie($id)
    {
        
        $exercises_categories = Exercises_Categorie::all();
        $exercises_categorie = Exercises_Categorie::where('id', $id)->first();

        return view('exercise.edit_categorie', compact('exercises_categories', 'exercises_categorie'));
    }

    public function updateCategorie(Request $request, Exercises_Categorie $exercisecategorie)
    {
        $exercisecategorie->update($request->all());

        return back();
    }

    public function compareDay($id, $name)
    {
        $exercises = Trainplans_Exercise::where('trainplan_id', $id)->where('day', $name)->get();
        foreach ($exercises as $key => $exercise) {

            $sets = $exercise->sets;
            $reps = $exercise->reps;
            $weight = $exercise->rm_value;

            $array[$key] = array('id' => $exercise->id, 'sets' => $sets, 'reps' => $reps, 'weight' => $weight, 'title' => $exercise->exercise->title);
        }

        return $array;
    }


    // jquery json requests

    public function Exercises_by_category(Request $request)
    {
        $exercise_categorie_id = $request->categorie_id;
        $exercises = DB::table('exercises')->select('id', 'title')->where('exercises_categorie_id', '=', $exercise_categorie_id)->get();

        return $exercises->toJson();
    }
}

