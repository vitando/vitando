<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\general;

use Auth;
use DB;
use Response;
use App\User;
use App\Note;
use App\Food;
use App\Foodplans_Recipe;
use App\Recipe;
use App\Recipes_Categorie;
use App\Recipes_Food;
use App\Users_Recipe;
use App\Http\Requests;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $recipes_categories = Recipes_Categorie::all();
        $recipes = Recipe::all();

        return view('recipe.index', compact('recipes_categories','recipes'));
    }

    public function show($id)
    {
        $recipes = Recipe::all();
        $recipe = Recipe::where('id', $id)->first();
        $notes = Note::where('note_type', 'recipe')->where('note_id', $id)->get();


        if(User::find(Auth::id())->recipes()->where('recipe_id', $id)->first()){
            $check = true;
        }else{
            $check = false;
        }

        return view('recipe.show', compact('recipes', 'recipe', 'check', 'notes'));
    }

    public function create(Request $request)
    {
        $i = 0;
        $amount_total = 0;
        $energy_kj_total = 0;
        $energy_kcal_total = 0;
        $protein_total = 0;
        $fat_total = 0;
        $sugar_total = 0;
        $carbs_total = 0;

        $recipe = new Recipe;

        $recipe->user_id = Auth::id();
        $recipe->title = $request->title;
        $recipe->recipes_categorie_id = $request->category;
        $recipe->description = $request->description;

        $recipe->url_vid = 'yolo';
        $recipe->url_pic = 'yolo';

        $recipe->save(); 

        foreach ($request->food as $food) {
            if ($i == 0) {
                $food_recipe = new Recipes_Food;
                $food_recipe->food_id = $food;
                $food_id = $food;
                $i++;
            }
            else if ($i == 1) {
                $food_recipe->amount = $food;
                $food_recipe->recipe_id = $recipe->id;
                $food_recipe->save();
                $i = 0;

                $amount_total = $amount_total + $food;
                $energy_kj_total = $energy_kj_total + (Food::find($food_id)->energy_kJ / 100 * $food);
                $energy_kcal_total = $energy_kcal_total + (Food::find($food_id)->energy_kcal / 100 * $food);
                $protein_total = $protein_total + (Food::find($food_id)->protein / 100 * $food);
                $fat_total = $fat_total + (Food::find($food_id)->fat_total / 100 * $food);
                $sugar_total = $sugar_total + (Food::find($food_id)->sugars / 100 * $food);
                $carbs_total = $carbs_total + (Food::find($food_id)->carbohydrates_available / 100 * $food);
            }
        }
        
        $recipe->amount = $amount_total;
        $recipe->energy_kj = $energy_kj_total;
        $recipe->energy_kcal = $energy_kcal_total;
        $recipe->protein = $protein_total;
        $recipe->fat = $fat_total;
        $recipe->sugar = $sugar_total;
        $recipe->carbs = $carbs_total;
        $recipe->save(); 

        return back();
    }
    public function edit($id)
    {
        $recipes_categories = Recipes_Categorie::all();
        $recipes = Recipe::all();
        $recipe = Recipe::where('id', $id)->first();

        return view('recipe.edit', compact('recipes_categories','recipes','recipe'));
    }

    public function update(Request $request, Recipe $recipe)
    {
        $i = 0;
        $amount_total = 0;
        $energy_kj_total = 0;
        $energy_kcal_total = 0;
        $protein_total = 0;
        $fat_total = 0;
        $sugar_total = 0;
        $carbs_total = 0;

        $recipe->update(['title' => $request->title, 'description' => $request->description, 'recipes_categorie_id' => $request->recipes_categorie_id]);


        foreach ($request->food as $food) {
            if ($i == 0) {
                if (Recipes_Food::find($food)) {
                    $recipe_food = Recipes_Food::find($food);
                    $i++;
                }else{
                    $recipe_food = new Recipes_Food;
                    $i++;
                }
            }
            else if ($i == 1) {
                $recipe_food->food_id = $food;
                $food_id = $food;
                $i++;
            }
            else if ($i == 2) {
                if ($food == "delete") {
                    $recipe_food->delete();
                    $i = 0;
                }else{
                $recipe_food->amount = $food;
                $recipe_food->recipe_id = $recipe->id;
                $recipe_food->save();
                $i = 0;

                $amount_total = $amount_total + $food;
                
                $energy_kj_total = $energy_kj_total + (Food::find($food_id)->energy_kJ / 100 * $food);
                $energy_kcal_total = $energy_kcal_total + (Food::find($food_id)->energy_kcal / 100 * $food);
                $protein_total = $protein_total + (Food::find($food_id)->protein / 100 * $food);
                $fat_total = $fat_total + (Food::find($food_id)->fat_total / 100 * $food);
                $sugar_total = $sugar_total + (Food::find($food_id)->sugars / 100 * $food);
                $carbs_total = $carbs_total + (Food::find($food_id)->carbohydrates_available / 100 * $food);                
                }

            }
        }
        
        $recipe->update([
            'amount' => $amount_total,
            'energy_kj' => $energy_kj_total,
            'energy_kcal' => $energy_kcal_total,
            'protein' => $protein_total,
            'fat' => $fat_total,
            'sugar' => $sugar_total,
            'carbs' => $carbs_total
        ]);

        return back();
    }

    public function createCategorie(Request $request)
    {
        $recipes_categorie = new Recipes_Categorie;

        $recipes_categorie->title = $request->title;
        $recipes_categorie->scope = $request->scope;
        $recipes_categorie->description = $request->description;

        $recipes_categorie->save(); 
        
        return back();
    }

    public function editCategorie($id)
    {
        
        $recipes_categories = Recipes_Categorie::all();
        $recipes_categorie = Recipes_Categorie::where('id', $id)->first();

        return view('recipe.edit_categorie', compact('recipes_categories', 'recipes_categorie'));
    }

    public function updateCategorie(Request $request, Recipes_Categorie $recipecategorie)
    {
        $recipecategorie->update($request->all());

        return back();
    }

    public function add_to_user($id)
    {
        $recipe = new Users_Recipe;

        $recipe->user_id = Auth::id();
        $recipe->recipe_id = $id;

        $recipe->save(); 
        
        return back();
    }

    public function note($id, Request $request)
    {
        $note = new Note;

        $note->user_id = Auth::id();

        $note->note_type = 'recipe';
        $note->note_id = $id;
        $note->value = $request->note;
        $note->save();
        
        return back();
    }

    // jquery json requests

    public function recipes_by_category(Request $request)
    {
        $recipe_categorie_id = $request->categorie_id;
        $recipes = DB::table('recipes')->select('id', 'title')->where('recipes_categorie_id', '=', $recipe_categorie_id)->get();

        return $recipes->toJson();
    }
    public function compareDay($id, $name)
    {
        $recipes = Foodplans_Recipe::where('foodplan_id', $id)->where('day', $name)->get();
        $count = $recipes->count();
        foreach ($recipes as $key => $recipe) {
            $percent_amount = (1 / $recipe->recipe->amount) * $recipe->amount;

            $kcal = $recipe->recipe->energy_kcal * $percent_amount;
            $fat = $recipe->recipe->fat * $percent_amount;
            $carbs = $recipe->recipe->carbs * $percent_amount;
            $protein = $recipe->recipe->protein * $percent_amount;

            $kcal = number_format($kcal, 2, ',','.');
            $fat = number_format($fat, 2, ',','.');
            $carbs = number_format($carbs, 2, ',','.');
            $protein = number_format($protein, 2, ',','.');

            $array[$key] = array('id' => $recipe->id, 'fat' => $fat, 'carbs' => $carbs, 'protein' => $protein, 'title' => $recipe->recipe->title, 'amount' => $recipe->amount);
            $general = new general;
            $compare = ($general->compareDay($array));

            $array[$key]['amount'] = $array[$key]['amount'] * $compare[$key]['amount'] / $count;
            $array[$key]['fat'] = $array[$key]['fat'] * $compare[$key]['amount'] / $count. "(" . $compare[$key]['fat'] .")";
            $array[$key]['carbs'] = $array[$key]['carbs'] * $compare[$key]['amount'] / $count. "(" . $compare[$key]['carbs'] .")";
            $array[$key]['protein'] = $array[$key]['protein'] * $compare[$key]['amount'] / $count. "(" . $compare[$key]['protein'] .")";
        }

        return $array;
    }
}
