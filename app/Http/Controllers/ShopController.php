<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Shop;

class ShopController extends Controller
{
    public function shop()
    {   
        $shops = Shop::all()->where('status', 'checked');
        return view('user.shop', compact('shops'));
    }

    public function registerShop(Request $request)
    {
        $shop = new Shop;
        $shop->user_id = Auth::id();
        $shop->title = $request->title;
        $shop->email = $request->email;
        $shop->firm = $request->firm;
        $shop->ust_id = $request->ust_id;
        $shop->street = $request->street;
        $shop->street_number = $request->street_number;
        $shop->street_add = $request->street_add;
        $shop->zip_code = $request->zip_code;
        $shop->town = $request->town;
        $shop->save();
        
        return back();
    }
}
