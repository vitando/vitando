<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Food;

class FoodController extends Controller
{
    public function find()
    {
        $foods = Food::all(['id', 'name']);
        return $foods->toJson();

    }
    public function find_by_id($id)
    {
        $foods = Food::all(['id', 'name'])->where('id', $id);
        
        return $foods->toJson();

    }
}
