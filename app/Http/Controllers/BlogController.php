<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;
use App\Note;
use App\User;
use App\Users_Blog;
use App\Blog;
use App\Blogs_Categorie;
use App\Http\Requests;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $blogs_categories = Blogs_Categorie::all();
        $blogs = Blog::all();

        return view('blog.index', compact('blogs_categories','blogs'));
    }

    public function show($id)
    {
        $blogs = Blog::all();
        $blog = Blog::where('id', $id)->first();
        $notes = Note::where('note_type', 'blog')->where('note_id', $id)->get();

        if(User::find(Auth::id())->blogs()->where('blog_id', $id)->first()){
            $check = true;
        }else{
            $check = false;
        }

        return view('blog.show', compact('blog', 'blogs', 'check', 'notes'));
    }

    public function create(Request $request)
    {
        $blog = new Blog;

        $blog->user_id = Auth::id();
        $blog->title = $request->title;
        $blog->blogs_categorie_id = $request->category;
        $blog->description = $request->description;
        $blog->text = $request->text;

        $blog->save();

        return back();
    }

    public function edit($id)
    {
        $blogs_categories = Blogs_Categorie::all();
        $blogs = Blog::all();
        $blog = Blog::where('id', $id)->first();

        return view('blog.edit', compact('blogs_categories','blogs','blog'));
    }

    public function update(Request $request, Blog $blog)
    {
        $blog->update($request->all());

        return back();
    }

    public function createCategorie(Request $request)
    {
        $blogs_categorie = new Blogs_Categorie;

        $blogs_categorie->title = $request->title;
        $blogs_categorie->scope = $request->scope;
        $blogs_categorie->description = $request->description;

        $blogs_categorie->save(); 
        
        return back();
    }

    public function editCategorie($id)
    {
        
        $blogs_categories = Blogs_Categorie::all();
        $blogs_categorie = Blogs_Categorie::where('id', $id)->first();

        return view('blog.edit_categorie', compact('blogs_categories', 'blogs_categorie'));
    }

    public function updateCategorie(Request $request, Blogs_Categorie $blogcategorie)
    {
        $blogcategorie->update($request->all());

        return back();
    }

    public function add_to_user($id)
    {
        $blog = new Users_Blog;

        $blog->user_id = Auth::id();
        $blog->blog_id = $id;

        $blog->save(); 
        
        return back();
    }

    public function note($id, Request $request)
    {
        $note = new Note;

        $note->user_id = Auth::id();

        $note->note_type = 'blog';
        $note->note_id = $id;
        $note->value = $request->note;
        $note->save();
        
        return back();
    }



    // jquery json requests

    public function blogs_by_category(Request $request)
    {
        $blog_categorie_id = $request->categorie_id;
        $blogs = DB::table('blogs')->select('id', 'title')->where('blogs_categorie_id', '=', $blog_categorie_id)->get();

        return $blogs->toJson();
    }
}