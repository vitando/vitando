<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Response;
use App\Shop;
use App\Shops_Trainer;
use App\Blog;
use App\Blogs_Categorie;
use App\User;
use App\Users_Categorie;
use App\Exercise;
use App\Exercises_Categorie;
use App\Trainplan;
use App\Trainplans_Categorie;
use App\Food;
use App\Foodplan;
use App\Foodplans_Categorie;
use App\Recipe;
use App\Aim;
use App\Users_Aim;
use App\Recipes_Categorie;
use App\Http\Requests;


class TrainerareaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('trainerarea.index');
    }

    public function shop()
    {
        $invites = Shops_Trainer::where('user_id', Auth::id())->where('status', 'invited')->get();
        $shop = Shop::where('user_id', Auth::id())->first();
        return view('trainerarea.shop', compact('shop', 'invites'));
    }
    public function shopInvite(Request $request)
    {
        $invite = new Shops_Trainer;
        $invite->user_id = $request->id[0];
        $invite->shop_id = Shop::where('user_id', Auth::id())->first()->id;
        $invite->save();
        return back();
    }

    public function aim()
    {
        $aims = Aim::all();
        return view('trainerarea.aim', compact('aims'));
    }

    public function aimEdit($id)
    {
        $aims = Aim::all();
        $aim = Aim::find($id);
        return view('calc.edit_aim', compact('aims','aim'));
    }

    public function aimCreate(Request $request)
    {
        $aim = new Aim;
        $aim->title = $request->title;
        $aim->user_id = Auth::id();
        $aim->description = $request->description;
        $aim->fat = $request->fat;
        $aim->carbs = $request->carbs;
        $aim->protein = $request->protein;
        $aim->save();

        return back();
    }
    public function aimUpdate($id, Request $request)
    {
        $aim = Aim::find($id);
        $aim->title = $request->title;
        $aim->user_id = Auth::id();
        $aim->description = $request->description;
        $aim->fat = $request->fat;
        $aim->carbs = $request->carbs;
        $aim->protein = $request->protein;
        $aim->save();

        return back();
    }
    public function foodplanCheck(Request $request)
    {
        $recipes = array();
        $id = 0;
        $i = 0;
        $kcal = 0;
        $protein = 0;
        $fat = 0;
        $carbs = 0;

        foreach ($request->recipes as $key => $recipe) {
            if ($i == 0) {
                $i++;
            }
            else if ($i == 1) {
                $id = $recipe;
                $recipes[$id] = Recipe::find($recipe);
                $i++;
            }
            else if ($i == 2) {
                $recipes[$id]->amount = $recipe;

                $kcal_result = $recipe / 100 * $recipes[$id]->energy_kcal;
                $protein_result = $recipe / 100 * $recipes[$id]->protein;
                $fat_result = $recipe / 100 * $recipes[$id]->fat;
                $carbs_result = $recipe / 100 * $recipes[$id]->carbs;

                $kcal = $kcal + $kcal_result;
                $protein = $protein + $protein_result;
                $fat = $fat + $fat_result;
                $carbs = $carbs + $carbs_result;

                $i++;
            }
            else if ($i == 3) {
                $i = 0;
            }
        }
        $total = $protein + $fat + $carbs;

        $protein_perCent = 100 / $total * $protein ;
        $fat_perCent = 100 / $total * $fat ;
        $carbs_perCent = 100 / $total * $carbs ;

        $aim = Aim::find($request->aim_id);

        return response()->json([
            'result_protein' => $protein_perCent,
            'result_fat' => $fat_perCent,
            'result_carbs' => $carbs_perCent,
            'aim_protein' => $aim->protein,
            'aim_fat' => $aim->fat,
            'aim_carbs' => $aim->carbs,
        ]);

    }

    public function show($id)
    {
        $aims = Aim::all();

        $exercises_categories = Exercises_Categorie::all();
        $exercises = Exercise::all();

        $trainplans_categories = Trainplans_Categorie::all();
        $trainplans = Trainplan::all();

        $recipes_categories = Recipes_Categorie::all();
        $recipes = Recipe::all();

        $blogs_categories = Blogs_Categorie::all();
        $blogs = Blog::all();

        $users_categories = Users_Categorie::all();
        $users = User::all();

        $foodplans_categories = Foodplans_Categorie::all();
        $foodplans = Foodplan::all();

        $id = 'trainerarea.' . $id;

        return view($id, compact('exercises_categories','exercises','trainplans_categories','trainplans','recipes_categories','recipes','blogs_categories','blogs','foodplans_categories','foodplans', 'users_categories', 'users', 'aims'));
    }

}
