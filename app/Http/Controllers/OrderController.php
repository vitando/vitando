<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function basket(Request $request)
    {
    	$basket = $request->session()->all();

    	return view('order.basket', compact('basket'));
    }
    public function addProduct(Request $request)
    {
    	$product = $request->session()->all();

    	return back();
    }
}
