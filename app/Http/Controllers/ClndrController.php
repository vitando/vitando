<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Trainplan;
use App\Trainplans_Exercise;

use App\Exercise;
use App\Users_Exercise;

use App\Foodplan;
use App\Foodplans_Recipe;

use App\Recipe;
use App\Users_Recipe;

class ClndrController extends Controller
{
    public function tpday($id)
    {
    	$trainplan = Trainplan::find($id);
        foreach ($trainplan->trainplans_exercises as $key => $exercise) {
            $day[$key] = $exercise->day;
        }
        $days = array_unique($day);
        return view('userarea.clndr.trainplan', compact('trainplan', 'days'));
    }
    public function fpday($id)
    {
    	$foodplan = Foodplan::find($id);
        foreach ($foodplan->foodplans_recipes as $key => $recipes) {
            $day[$key] = $recipes->day;
        }
        $days = array_unique($day);

        return view('userarea.clndr.foodplan', compact('foodplan', 'days'));
    }
    public function exercise($id)
    {
    	$exercise = Trainplans_Exercise::find($id);

        return view('userarea.clndr.exercise', compact('exercise'));
    }
    public function recipe($id)
    {
    	$recipe = Foodplans_Recipe::find($id);

        return view('userarea.clndr.recipe', compact('recipe'));
    }
}
