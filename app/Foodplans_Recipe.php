<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foodplans_recipe extends Model
{
	protected $fillable = ['foodplan_id', 'recipe_id', 'title', 'amount', 'description', 'day', 'price', 'duration', 'foodplans_categorie_id'];

    public function foodplan()
    {
    	return $this->belongsTo(Foodplan::class, 'foodplan_id');
    }
    public function recipe()
    {
    	return $this->belongsTo(Recipe::class, 'recipe_id');
    }
}
