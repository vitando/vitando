<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainplan extends Model
{

	protected $fillable = ['trainplan_id', 'exercise_id', 'title', 'rm_value', 'sets', 'reps', 'description', 'day', 'price', 'duration', 'trainplans_categorie_id'];

	public function exercises()
	{
		return $this->hasMany(Exercise::class);
	}

	public function trainplans_exercises()
	{
		return $this->hasMany(Trainplans_Exercise::class);
	}

	public function trainplans_categories()
	{
		return $this->belongsTo(Trainplans_Categorie::class, 'trainplans_categorie_id');
	}
    
	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}

}
