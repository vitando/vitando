<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_event extends Model
{
	protected $fillable = ['event_type', 'event_id', 'event_name', 'event_date'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
