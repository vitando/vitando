<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_addr extends Model
{
    protected $fillable = [
    'vname', 'nname', 'email', 'user_id', 'type', 'title', 'firm', 'street', 'street_number', 'street_add', 'town', 'zip_code'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
