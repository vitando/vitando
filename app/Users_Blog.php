<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_blog extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function blog()
	{
		return $this->belongsTo(Blog::class, 'blog_id');
	}
}
