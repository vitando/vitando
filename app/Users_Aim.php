<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_aim extends Model
{
	protected $fillable = ['aim_id', 'user_id', 'note', 'weight'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}
}
