<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainplans_exercise extends Model
{	

	protected $fillable = ['trainplan_id', 'exercise_id', 'title', 'rm_value', 'sets', 'reps', 'description', 'day', 'price', 'duration', 'trainplans_categorie_id'];

    public function trainplan()
    {
    	return $this->belongsTo(Trainplan::class, 'trainplan_id');
    }
    public function exercise()
    {
    	return $this->belongsTo(Exercise::class, 'exercise_id');
    }
}
