<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercises_categorie extends Model
{
	protected $fillable = ['scope', 'description', 'title'];

	public function exercises()
	{
		return $this->hasMany(Exercise::class);
	}
	public function exercises_categories()
	{
		return $this->belongsTo(Exercises_Categorie::class, 'scope');
	}
}
