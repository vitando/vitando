<?php

namespace App\Traits;

use Illuminate\Http\Request;

use Auth;
use Response;
use App\Users_Aim;
use App\Users_Pal;
use App\Http\Requests;
use App\Traits\general;

class general
{
    private $aim;
    private $pal;

    public function __construct()
    {
        $users_aim = Users_Aim::all();
        $get_aim = $users_aim->where('user_id', Auth::id())->first();
        $this->aim['fat'] = $get_aim->aim->fat;
        $this->aim['carbs'] = $get_aim->aim->carbs;
        $this->aim['protein'] = $get_aim->aim->protein;

        $users_pal = Users_Pal::where('user_id', Auth::id())->first();
        $this->pal = $users_pal->energy_total;
    }
    public function checkRange($val, $range) {
        if ($val >= $range * -1 && $val <= $range) {
            return true;
        } else {
            return false;
        }
    }

    public function compare($data)
    {
        $data = abs($data);
        $general = new general;
        if ($general->checkRange($data, 5)) {
            return('nice');
        }else if($general->checkRange($data, 10)){
            return('not so nice');
        }else if($general->checkRange($data, 15)){
            return('bad');
        }else{
            return('extremely bad');
        }
    }

    public function compareDay($array)
    {
    	foreach ($array as $key => $day) {
            if ($day['fat'] != 0) {
                $energy_fat = $day['fat'] * 9.3;
                $energy_carbs = $day['carbs'] * 4.1;
                $energy_protein = $day['protein'] * 4.1;

                $total = $energy_fat + $energy_carbs + $energy_protein;

        		$fat = 100 / $total * $energy_fat;
        		$carbs = 100 / $total * $energy_carbs;
        		$protein = 100 / $total * $energy_protein;

                $optimize = $this->pal / $total;

                // --- Berechnung prozentualer Unterschied ---
    	    	$fat = $fat - $this->aim['fat'];
    	    	$carbs = $carbs - $this->aim['carbs'];
    	    	$protein = $protein - $this->aim['protein'];

    	    	$general = new general;
    	    	$fat_result = ($general->compare($fat));
    	    	$carbs_result = ($general->compare($carbs));
    	    	$protein_result = ($general->compare($protein));

    	    	$array[$key] = ['amount' => $optimize, 'fat' => $fat_result, 'carbs' => $carbs_result, 'protein' => $protein_result];
            }
    	}
    return($array);
    }

    public function calcAim($data)
    {
        $data = abs($data);
        $aim_fat = $data * $this->aim['fat'] / 100;
        $aim_carbs = $data * $this->aim['carbs'] / 100;
        $aim_protein = $data * $this->aim['protein'] / 100;

        $fat = $aim_fat / 9.3;
        $carbs = $aim_carbs / 4.1;
        $protein = $aim_protein / 4.1;

        $aim = ['fat' => $fat, 'carbs' => $carbs, 'protein' => $protein];
        return($aim);
    }

    public function optimize($data)
    {

        $opti_percent = (1 / $kcal) * $this->pal;

        $data = abs($data);
        $opti_fat = $data * $this->aim['fat'] / 100;
        $opti_carbs = $data * $this->aim['carbs'] / 100;
        $opti_protein = $data * $this->aim['protein'] / 100;

        $opti = ['fat' => $fat, 'carbs' => $carbs, 'protein' => $protein];
        return($aim);
    }
}