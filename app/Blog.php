<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $fillable = ['blogs_categorie_id', 'description', 'title', 'text'];
	
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}	
	public function blogs_categories()
	{
		return $this->belongsTo(Blogs_Categorie::class, 'blogs_categorie_id');
	}
}
