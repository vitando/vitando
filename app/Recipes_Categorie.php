<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipes_categorie extends Model
{

	protected $fillable = ['recipe_id', 'food_id', 'title', 'energy_kj', 'energy_kcal', 'protein', 'fat', 'sugar', 'carbs', 'description', 'recipes_categorie_id'];
	
	public function recipes()
	{
		return $this->hasMany(Recipe::class);
	}
	public function recipes_categories()
	{
		return $this->belongsTo(Recipes_Categorie::class, 'scope');
	}
}
