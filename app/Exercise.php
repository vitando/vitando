<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = ['exercises_categorie_id', 'description', 'title'];

	public function exercises_categories()
	{
		return $this->belongsTo(Exercises_Categorie::class, 'exercises_categorie_id');
	}

	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}
}

