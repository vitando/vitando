<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_exercise extends Model
{
	protected $fillable = ['rm_value', 'train_value'];

	use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionEnabled = true;
    
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function exercise()
	{
		return $this->belongsTo(Exercise::class, 'exercise_id');
	}
}
