<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_categorie extends Model
{
	protected $fillable = ['scope', 'description', 'title'];

	public function users()
	{
		return $this->hasMany(User::class);
	}
	public function users_categories()
	{
		return $this->belongsTo(Users_Categorie::class, 'scope');
	}
}
