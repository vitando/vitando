<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Hootlex\Friendships\Traits\Friendable;

class User extends Authenticatable
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionEnabled = true;

    use Friendable;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'bday', 'gender', 'size', 'weight', 'instatoken'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function exercises()
    {
        return $this->hasMany(Users_exercise::class);
    }
    public function trainplans()
    {
        return $this->hasMany(Users_trainplan::class);
    }
    public function foodplans()
    {
        return $this->hasMany(Users_foodplan::class);
    }
    public function users_cats()
    {
        return $this->hasMany(Users_Cat::class);
    }
    public function blogs()
    {
        return $this->hasMany(Users_Blog::class);
    }
    public function recipes()
    {
        return $this->hasMany(Users_Recipe::class);
    }
    public function addrs()
    {
        return $this->hasMany(Users_Addr::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }
}
