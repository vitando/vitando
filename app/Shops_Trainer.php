<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shops_trainer extends Model
{
    protected $fillable = [
    'user_id', 'shop_id', 'status'];

	public function shop()
	{
		return $this->belongsTo(Shop::class, 'shop_id');
	}
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
