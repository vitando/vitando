<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
	protected $fillable = ['value'];

	public function users()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
