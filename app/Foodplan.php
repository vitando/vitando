<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foodplan extends Model
{

	protected $fillable = ['recipe_id', 'food_id', 'title', 'energy_kj', 'energy_kcal', 'protein', 'fat', 'sugar', 'carbs', 'description', 'recipes_categorie_id'];

	public function recipes()
	{
		return $this->hasMany(Recipe::class);
	}

	public function foodplans_recipes()
	{
		return $this->hasMany(Foodplans_Recipe::class);
	}

	public function foodplans_categories()
	{
		return $this->belongsTo(Foodplans_Categorie::class, 'foodplans_categorie_id');
	}

	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}
}
