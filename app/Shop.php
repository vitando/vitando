<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
    'email', 'user_id', 'type', 'title', 'firm', 'ust_id', 'street', 'street_number', 'street_add', 'town', 'zip_code'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function trainer()
	{
		return $this->hasManny(Shops_Trainer::class);
	}
}
