<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainplans_categorie extends Model
{
	protected $fillable = ['scope', 'description', 'title'];

	public function trainplans()
	{
		return $this->hasMany(Trainplan::class);
	}
	public function trainplans_categories()
	{
		return $this->belongsTo(Trainplans_Categorie::class, 'scope');
	}
}
