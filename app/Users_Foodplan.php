<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_foodplan extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function foodplan()
	{
		return $this->belongsTo(Foodplan::class, 'foodplan_id');
	}
}
