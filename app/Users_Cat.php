<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_cat extends Model
{
    public function users_categories()
    {
		return $this->belongsTo(Users_categorie::class, 'cats_id');
    }
}