<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogs_categorie extends Model
{
   protected $fillable = ['scope', 'description', 'title'];

	public function blogs_categories()
	{
		return $this->belongsTo(Blogs_Categorie::class, 'scope');
	}
}
