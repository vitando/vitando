<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_trainplan extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function trainplan()
	{
		return $this->belongsTo(Trainplan::class, 'trainplan_id');
	}
}
