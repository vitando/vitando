<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
	protected $fillable = ['recipe_id', 'amount', 'food_id', 'title', 'energy_kj', 'energy_kcal', 'protein', 'fat', 'sugar', 'carbs', 'description', 'recipes_categorie_id'];

	public function recipes_categories()
	{
		return $this->belongsTo(Recipes_Categorie::class, 'recipes_categorie_id');
	}

	public function aim()
	{
		return $this->belongsTo(Aim::class, 'aim_id');
	}

	public function foods()
	{
		return $this->hasMany(Recipes_Food::class);
	}

}
