<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipes_food extends Model
{
	protected $fillable = ['amount', 'recipe_id', 'food_id'];

	public function recipes()
	{
		return $this->belongsTo(Recipe::class, 'recipe_id');
	}
	public function foods()
	{
		return $this->belongsTo(Food::class, 'food_id');
	}

}
