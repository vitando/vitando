<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aim extends Model
{
	protected $fillable = ['title', 'fat', 'carbs', 'protein'];
	
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}	
}
