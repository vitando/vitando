<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_recipe extends Model
{
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	public function recipe()
	{
		return $this->belongsTo(Recipe::class, 'recipe_id');
	}
}
