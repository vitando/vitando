<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Aim;
use App\User;
use App\Shop;
use App\Users_Addr;
use App\Exercise;
use App\Exercises_Categorie;
use App\Trainplans_Categorie;
use App\Foodplans_Categorie;
use App\Recipes_Categorie;
use App\Users_Categorie;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aim = new Aim;
        $aim->title = 'Abnehmen';
        $aim->user_id = '1';
        $aim->description = 'Einfach chillen';
        $aim->fat = 20;
        $aim->carbs = 30;
        $aim->protein = 20;
        $aim->save();

        $atzen = new User;
        $atzen->id = 1;
        $atzen->name = 'Atzen';
        $atzen->type = 'trainer';
        $atzen->email = 'smolen.art@gmail.com';
        $atzen->password = '$2y$10$nb4pisxMhSzR6Ix/rQUGeOhwM52/J7Yi.kOTGMuWV.dEfHRqoWRUy';
        $atzen->instatoken = '4531956709.60d2b9f.b6ff075c156c4210b59c28a685e52486';
        $atzen->bday = '1988-01-22';
        $atzen->gender = 0;
        $atzen->size = 167.00;
        $atzen->weight = 62.00;
        $atzen->save();

        $atzen = new User;
        $atzen->id = 2;
        $atzen->name = 'TeQ';
        $atzen->type = 'trainer';
        $atzen->email = 'tconrad@gmx.net';
        $atzen->password = '$2y$10$4h7miDQvPkElPzh.Oo1QNOevkmDedPMe1i3s1fMIlrpiTB3S6yQEm';
        $atzen->instatoken = '225437757.60d2b9f.29e80898765e4f18ade6ab55ee0064c8';
        $atzen->bday = '07.11.1987';
        $atzen->gender = 0;
        $atzen->size = 186.00;
        $atzen->weight = 82.00;
        $atzen->save();

        $atzen = new Users_Addr;
        $atzen->id = 1;
        $atzen->user_id = 1;
        $atzen->type = 'default';
        $atzen->email = 'smolen.art@gmail.com';
        $atzen->vname = 'Artur';
        $atzen->nname = 'Smolen';
        $atzen->title = 'Dr.';
        $atzen->street = 'Beselerstr.';
        $atzen->street_add = '0';
        $atzen->street_number = '99';
        $atzen->zip_code = '50354';
        $atzen->town = 'Hürth';
        $atzen->save();

        $teq = new Users_Addr;
        $teq->id = 2;
        $teq->user_id = 2;
        $teq->type = 'default';
        $teq->email = 'tconrad@gmx.net';
        $teq->vname = 'Timo';
        $teq->nname = 'Conrad';
        $teq->title = 'Herr';
        $teq->street = 'Kölnstr.';
        $teq->street_add = '0';
        $teq->street_number = '27';
        $teq->zip_code = '50354';
        $teq->town = 'Hürth';
        $teq->save();

        $shop = new Shop;
        $shop->user_id = 1;
        $shop->title = 'vitando';
        $shop->email = 'shop@vitando.de';
        $shop->firm = 'vitando GmbH';
        $shop->ust_id = 'DE1234567';
        $shop->street = 'Hürtherstr.';
        $shop->street_number = 99;
        $shop->street_add = 'epiC';
        $shop->zip_code = '50354';
        $shop->town = 'Hürth';
        $shop->status = 'checked';
        $shop->save();

        $exercises_categorie = new Exercises_Categorie;

        $exercises_categorie->title = 'Kategorie für Übung';
        $exercises_categorie->scope = 0;
        $exercises_categorie->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $exercises_categorie->save(); 

        $exercise = new Exercise;

        $exercise->user_id = 1;
        $exercise->title = 'Bankdrücken';
        $exercise->exercises_categorie_id = 1;
        $exercise->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $exercise->save();

        $exercise = new Exercise;

        $exercise->user_id = 1;
        $exercise->title = 'Kreuzheben';
        $exercise->exercises_categorie_id = 1;
        $exercise->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $exercise->save(); 

        $trainplans_categorie = new Trainplans_Categorie;

        $trainplans_categorie->title = 'Kategorie für Trainingsplan';
        $trainplans_categorie->scope = 0;
        $trainplans_categorie->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $trainplans_categorie->save(); 


        $foodplans_categorie = new Foodplans_Categorie;

        $foodplans_categorie->title = 'Kategorie für Ernährungspläne';
        $foodplans_categorie->scope = 0;
        $foodplans_categorie->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $foodplans_categorie->save(); 

        $recipes_categorie = new Recipes_Categorie;
        $recipes_categorie->title = 'Kategorie für Rezept';
        $recipes_categorie->scope = 0;
        $recipes_categorie->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';
        $recipes_categorie->save(); 

        $users_categorie = new Users_Categorie;

        $users_categorie->title = 'Kategorie für Trainer';
        $users_categorie->scope = 0;
        $users_categorie->description = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata';

        $users_categorie->save(); 
    }
}
