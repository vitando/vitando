<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foodplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('aim_id');
            $table->integer('foodplans_categorie_id');
            $table->string('title', 100);
            $table->string('type');
            $table->text('description');
            $table->integer('duration');
            $table->float('price')->default('0');
            $table->timestamps();
        });
        Schema::create('foodplans_recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('foodplan_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->string('amount');
            $table->string('description')->default('0');
            $table->string('day');
            $table->timestamps();
        });
        Schema::table('foodplans_recipes', function($table) {
            $table->foreign('foodplan_id')->references('id')->on('foodplans')->onDelete('cascade');
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
    
        Schema::create('foodplans_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('scope')->unsigned();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foodplans_recipes');
        Schema::dropIfExists('foodplans_categories');
        Schema::dropIfExists('foodplans');
    }
}
