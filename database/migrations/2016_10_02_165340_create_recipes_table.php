<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('aim_id')->default('0');
            $table->integer('recipes_categorie_id');
            $table->text('title');
            $table->text('description');
            $table->float('amount')->default('0');
            $table->float('energy_kj')->default('0');
            $table->float('energy_kcal')->default('0');
            $table->float('protein')->default('0');
            $table->float('fat')->default('0');
            $table->float('sugar')->default('0');
            $table->float('carbs')->default('0');
            $table->text('url_vid');
            $table->text('url_pic');
            $table->timestamps();
        });
        Schema::create('recipes_foods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id')->unsigned();
            $table->integer('food_id')->unsigned();
            $table->float('amount');
            $table->timestamps();
        });
       Schema::table('recipes_foods', function($table) {
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
        });
        Schema::create('recipes_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('scope');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes_foods');
        Schema::dropIfExists('recipes_categories');
        Schema::dropIfExists('recipes');
    }
}
