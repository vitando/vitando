<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('status')->default('disable');
            $table->string('title');
            $table->string('email')->unique();
            $table->string('firm')->default('0');
            $table->string('ust_id')->default('0');
            $table->string('street')->default('0');
            $table->string('street_add')->default('0');
            $table->string('street_number')->default('0');
            $table->string('zip_code')->default('0');
            $table->string('town')->default('0');
            $table->timestamps();
        });

        Schema::create('shops_trainers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('shop_id');
            $table->string('status')->default('invited');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
        Schema::dropIfExists('shops_trainers');
    }
}
