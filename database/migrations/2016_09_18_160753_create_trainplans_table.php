<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainplansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('aim_id')->default('0');
            $table->integer('trainplans_categorie_id');
            $table->string('title', 100);
            $table->string('type');
            $table->text('description');
            $table->integer('duration');
            $table->float('price');
            $table->timestamps();
        });
        Schema::create('trainplans_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trainplan_id')->unsigned();
            $table->integer('exercise_id')->unsigned();
            $table->integer('rm_value');
            $table->integer('sets');
            $table->integer('reps');
            $table->text('description');
            $table->text('day');
            $table->timestamps();
        });
       Schema::table('trainplans_exercises', function($table) {
            $table->foreign('trainplan_id')->references('id')->on('trainplans')->onDelete('cascade');
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
       });
        Schema::create('trainplans_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->integer('scope')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainplans_exercises');
        Schema::dropIfExists('trainplans_categories');
        Schema::dropIfExists('trainplans');
    }
}
