<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('bday')->default('0');
            $table->string('instatoken')->default('0');
            $table->integer('gender')->default('0');
            $table->float('size')->default('0');
            $table->float('weight')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('users_addrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('type');
            $table->string('email')->unique();
            $table->string('vname')->default('0');
            $table->string('nname')->default('0');
            $table->string('title')->default('0');
            $table->string('firm')->default('0');
            $table->string('ust_id')->default('0');
            $table->string('street')->default('0');
            $table->string('street_add')->default('0');
            $table->string('street_number')->default('0');
            $table->string('zip_code')->default('0');
            $table->string('town')->default('0');
            $table->timestamps();
        });
        Schema::create('users_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('scope');
            $table->timestamps();
        });

        Schema::create('users_cats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('cats_id')->unsigned();
            $table->integer('value');
            $table->timestamps();
        });

        Schema::create('users_pals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->float('weight')->default('0');
            $table->float('age')->default('0');
            $table->float('energy');
            $table->float('pal_day');
            $table->float('energy_total');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::create('users_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('exercise_id')->unsigned();
            $table->integer('rm_value')->default('70');
            $table->integer('train_value')->default('0');
            $table->timestamps();
        });
        Schema::table('users_exercises', function($table) {
            $table->foreign('exercise_id')->references('id')->on('exercises')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('users_trainplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('trainplan_id')->unsigned();
            $table->integer('order_id')->default('0');
            $table->timestamps();
        });
        Schema::table('users_trainplans', function($table) {
            $table->foreign('trainplan_id')->references('id')->on('trainplans')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('users_foodplans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('foodplan_id')->unsigned();
            $table->integer('order_id')->default('0');
            $table->timestamps();
        });
        Schema::table('users_foodplans', function($table) {
            $table->foreign('foodplan_id')->references('id')->on('foodplans')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('users_blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('blog_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('users_recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('recipe_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('users_recipes', function($table) {
            $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('users_aims', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('aim_id')->unsigned();
            $table->text('note');
            $table->integer('weight')->default('0');
            $table->timestamps();
        });
        Schema::table('users_aims', function($table) {
            $table->foreign('aim_id')->references('id')->on('aims')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        
        Schema::create('users_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->string('event_type');
            $table->string('event_name');
            $table->string('event_date');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_addrs');
        Schema::dropIfExists('users_categories');
        Schema::dropIfExists('users_cats');
        Schema::dropIfExists('users_pals');
        Schema::dropIfExists('users_exercises');
        Schema::dropIfExists('users_trainplans');
        Schema::dropIfExists('users_foodplans');
        Schema::dropIfExists('users_blogs');
        Schema::dropIfExists('users_recipes');
        Schema::dropIfExists('users_aims');
        Schema::dropIfExists('users_events');
    }
}
