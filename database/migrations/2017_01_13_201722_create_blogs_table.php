<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('link_id')->default('0');
            $table->string('link_type')->default('0');
            $table->string('blogs_categorie_id');
            $table->string('title');
            $table->string('description');
            $table->longText('text');
            $table->string('pic_url')->default('0');
            $table->string('vid_url')->default('0');
            $table->timestamps();
        });
        Schema::create('blogs_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('scope');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
        Schema::dropIfExists('blogs_categories');
    }
}
