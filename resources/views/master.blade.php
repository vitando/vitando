<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="verification" content="de443ca0852930afc4776ce41e79796f" />
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a href="{{ url('/') }}">

                    <img src="/pics/logo_v3.png" class="img-responsive" alt="Image" width="200">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                    <li><a href="/trainplan/index">Träningspläne</a></li>
                    <li><a href="/foodplan/index">Ernährungspläne</a></li>
                    <li><a href="/exercise/index">Übungen</a></li>
                    <li><a href="/recipe/index">Rezepte</a></li>
                    <!-- <li><a href="/blog/index">Blogs</a></li> -->
                    <!-- <li><a href="/trainer/index">Trainer</a></li> -->
                    <!-- <li><a href="/shop/index">Shops</a></li> -->
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                        <!-- <li><a href="{{ url('/basket') }}">Warenkorb</a></li> -->
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li><a href="/userarea">{{ Auth::user()->name }}</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')

    <!-- Footer -->
    <div class="panel panel-success" id="footer">

      <div class="panel-body text-center" id="footer_b">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita aliquid soluta, quod ad, iste quasi reiciendis illo voluptate minus est ex provident dolore.
      </div>

      <div class="panel-footer ang clear" id="footer">
        <div id="overlayh"></div>
          <div class="container">
            <br>
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                  <b>Social Media</b><br>
                  <small>
                    <a href="">Facebook</a><br>
                    <a href="">Twitter</a><br>
                    <a href="">Instagram</a>
                  </small>
              </div>

              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <b>Partner</b><br>
                  <small>
                    <a href="">Link 1</a><br>
                    <a href="">Link 2</a><br>
                    <a href="">Link 3</a><br>
                  </small>
              </div>

              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-right">
                <b>Rechtliches</b> <br>
                  <small>
                    <a href="?link=law">Impressum</a> <br>
                    <a href="?link=law">AGB's</a> <br>
                    <a href="?link=law">Datenschutz</a> <br>
                    <a href="">Kontakt</a>
                  </small>
              </div>

              <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <small>
                  <br>
                  <b>Copyright ©2015 vitando.de Alle Rechte vorbehalten.</b>
                </small>
              </div>

        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="/js/addons.js"></script>
    <script src="/js/all.js"></script>
    @yield('script')
</body>
</html>
