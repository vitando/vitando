@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <!-- !!! Kategorie für Übung erstellen !!! -->
                    <form action="gym/categories" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <legend>Kategorie für Übung erstellen</legend>
                        <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <label for="">Titel</label>
                            <input type="text" class="form-control" name="title" placeholder="" required>
                        </div>                    
                        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Über Kategorie</label>
                            <select name="scope" class="form-control" required="required">
                                <option value="">Keine</option>
                                @foreach($exercises_categories as $exercises_categorie)
                                        <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                                @endforeach
                            </select>
                        </div>                    
                        <div class="form-group col-sm-12">
                            <label for="">Beschreibung</label>
                            <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
                        </div>                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    <!-- !!! Übung erstellen !!! -->

                    <form action="gym/exercices" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <legend>Übung erstellen</legend>
                  
                        <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <label for="">Titel</label>
                            <input type="text" class="form-control" name="title" placeholder="">
                        </div>                    
                        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Kategorie</label>
                            <select name="category" class="form-control" required="required">
                                <option value="">Keine</option>
                                @foreach($exercises_categories as $exercises_categorie)
                                        <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                                @endforeach
                            </select>
                        </div>                    
                        <div class="form-group col-sm-12">
                            <label for="">Beschreibung</label>
                            <textarea name="description" class="form-control" rows="5" required="required"></textarea>
                        </div>                    
                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                    <!-- !!! Trainingspläne erstellen !!! -->

                    <form action="gym/exercices" method="POST" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <legend>Trainingsplan erstellen</legend>
                  
                        <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <label for="">Titel</label>
                            <input type="text" class="form-control" name="title" placeholder="">
                        </div>                   
                        <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Kategorie</label>
                            <select name="category" class="form-control" required="required">
                                <option value="">Keine</option>
                                @foreach($exercises_categories as $exercises_categorie)
                                        <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                                @endforeach
                            </select>
                        </div>                    
                        <div class="form-group col-sm-12">
                            <label for="">Beschreibung</label>
                            <textarea name="description" class="form-control" rows="5" required="required"></textarea>
                        </div>                    
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center trainplan-day">
                            <div class="row form-group">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                                    <select name="category" class="form-control" required>
                                        <option value="0">Montag</option>
                                        <option value="1">Dienstag</option>
                                        <option value="2">Mittwoch</option>
                                        <option value="3">Donnerstag</option>
                                        <option value="4">Freitag</option>
                                        <option value="5">Samstag</option>
                                        <option value="6">Sonntag</option>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-trainplan-day" disabled></button>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <label for="">Kategorie</label>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <label for="">Übung</label>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <label for=""><br></label>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <label for="">Sets</label>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <label for="">Reps</label>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <label for="">RM</label>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <label for="">Beschreibung / Notiz</label>
                                </div>                                
                            </div>
                            <div class="row form-group trainplan-exercise">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <select id="exercise-categorie" class="form-control exercise-categorie" required="required">
                                        <option value="">Keine</option>
                                        @foreach($exercises_categories as $exercises_categorie)
                                                <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <select name="exercise" class="form-control exercise-with-categorie" required="required" disabled></select>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <input type="number" class="form-control" name="sets" placeholder="">
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <input type="number" class="form-control" name="reps" placeholder="">
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <input type="number" class="form-control" name="max_rm" placeholder="">
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="text" class="form-control" name="desciption" placeholder="">
                                </div>
                            </div>
                            <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-exercise"></button>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
