@extends('user.layout')

@section('layoutcontent')

<!-- !!! User in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für User/Trainer</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users_categories as $ousers_categorie)
        <tr>
            <td>{{$ousers_categorie->title}} <a href="/usercategorie/{{$ousers_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $ousers_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$ousers_categorie->users_categories()->first()->title}}</td>
            @endif
            <td>{{$ousers_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorie für Übung bearbeiten !!! -->

<form action="/usercategorie/{{$users_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für User/Trainer bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$users_categorie->title}}" placeholder="{{$users_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($users_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($users_categories as $ausers_categorie)
                    <option value="{{$ausers_categorie->id}}">{{$ausers_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($users_categories as $ausers_categorie)
                    @if($ausers_categorie->id == $users_categorie->scope)
                        <option value="{{$ausers_categorie->id}}" selected>{{$ausers_categorie->title}}</option>
                    @else
                        <option value="{{$ausers_categorie->id}}">{{$ausers_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$users_categorie->description}}</textarea>
    </div>                    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
