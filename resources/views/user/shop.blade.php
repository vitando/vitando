@extends('user.layout')
@section('layoutcontenttitle')
<h4>Shops</h4>
@endsection
@section('layoutcontent')
<!-- !!! Rezepte in Tabelle anzeigen !!! -->
    @foreach($shops as $shop)
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="thumbnail">
            <img src="http://placehold.it/350x150" alt="">
            <div class="caption">
                <h3 class="text-center">{{$shop->title}}</h3>
                <p>
                    ...
                </p>
                <p>
                    <a href="#" class="btn btn-primary">Action</a>
                    <a href="#" class="btn btn-default">Action</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
@endsection

