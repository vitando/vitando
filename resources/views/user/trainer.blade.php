@extends('user.layout')
@section('layoutcontenttitle')
<h4>Trainer</h4>
@endsection
@section('layoutcontent')
<!-- !!! Rezepte in Tabelle anzeigen !!! -->
    @foreach($trainers as $trainer)
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
        <div class="thumbnail">
            <img src="http://placehold.it/350x150" alt="">
            <div class="caption">
                <h3 class="text-center">{{$trainer->name}}</h3>
                <p>
                    ...
                </p>
                <p>
                    <a href="/trainer/{{$trainer->id}}/show" class="btn btn-primary">Profil</a>
                    <a href="/user/befriend/{{$trainer->id}}"" class="btn btn-default">Freundschafts-Anfrage</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
@endsection

