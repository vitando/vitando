@extends('user.layout')
@section('layoutcontenttitle')
<h4>Trainer: {{$trainer->name}}</h4>
@endsection
@section('layoutcontent')
<!-- !!! Rezepte in Tabelle anzeigen !!! -->
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <ul class="list-group">
      <li class="list-group-item"><img src="http://placehold.it/550x350" class="img-responsive" alt="Image"></li>
      <li class="list-group-item">Vorname: {{$trainer->addrs()->first()->vname}}</li>
      <li class="list-group-item">Nachname: {{$trainer->addrs()->first()->nname}}</li>
      <li class="list-group-item">Ort/Umgebung: {{$trainer->addrs()->first()->town}}</li>
      <li class="list-group-item">Kategorien: @foreach($cats as $cat) @if($loop->last) {{$cat->users_categories->title}} @else {{$cat->users_categories->title}}, @endif @endforeach</li>
    </ul>
</div>

<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
@if(!empty($trainer->instatoken))
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
        <span class="btn btn-large btn-block btn-success">Mit Instagramm verbunden!</span><br>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
        <div id="insta"></div>
    <br>
    </div>
    @section('script')
        <script>
            var items = '';
            $.ajax({
              method: "GET",
              url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token={{ $trainer->instatoken }}&callback=?',
              async: false,
              dataType: "jsonp",
              success: function (data) {
                $.each(data.data, function(id, data) {
                    items += '<img src="' + data.images.low_resolution.url + '" class="img" alt="Image">';
                });
                $( "#insta" ).append(items);
              }
            });
        </script>
    @endsection
@endif
</div>

@endsection

