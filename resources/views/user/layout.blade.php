@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading text-center"><b>@yield('layoutcontenttitle')</b></div>
                <div class="panel-body">
                @yield('layoutcontent')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
