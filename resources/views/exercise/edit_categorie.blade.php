@extends('exercise.layout')

@section('layoutcontent')

<!-- !!! Übungen in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für Übungen</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exercises_categories as $oexercises_categorie)
        <tr>
            <td>{{$oexercises_categorie->title}} <a href="/exercisecategorie/{{$oexercises_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $oexercises_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$oexercises_categorie->exercises_categories()->first()->title}}</td>
            @endif
            <td>{{$oexercises_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorie für Übung bearbeiten !!! -->

<form action="/exercisecategorie/{{$exercises_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für Übung bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$exercises_categorie->title}}" placeholder="{{$exercises_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($exercises_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($exercises_categories as $aexercises_categorie)
                    <option value="{{$aexercises_categorie->id}}">{{$aexercises_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($exercises_categories as $aexercises_categorie)
                    @if($aexercises_categorie->id == $exercises_categorie->scope)
                        <option value="{{$aexercises_categorie->id}}" selected>{{$aexercises_categorie->title}}</option>
                    @else
                        <option value="{{$aexercises_categorie->id}}">{{$aexercises_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$exercises_categorie->description}}</textarea>
    </div>                    
    
    

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
