@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Übung bearbeiten: {{$user_exercise->exercise->title}}</h4>
@endsection
@section('layoutcontent')
<!-- !!! RM Modal !!! -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a class="btn btn-success" data-toggle="modal" href='#modal-id'>RM-Tabelle</a><br><br>
</div>
<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center"><b>RM bestimmen</b></h4>
            </div>
            <div class="modal-body">
                <div class="text-center">Im Folgenden wird eine Tabelle dargestellt, die eine Anzahl von Wiederholungen einen bestimmten prozentualen Wert zuordnet.
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Wiederholungen</th><th>RM-Wert nach Ruhne (1992)</th><th>RM-Wert nach Brzycki (1998)</th><th>RM-Wert nach Baechle (2000)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td><td>100%</td><td>100%</td><td>100%</td>
                        </tr>
                        <tr>
                            <td>2</td><td>95%</td><td>95%</td><td>95%</td>
                        </tr>
                        <tr>
                            <td>3-4</td><td>90%</td><td>90%-88%</td><td>93%-90%</td>
                        </tr>
                        <tr>
                            <td>5-6</td><td>85%</td><td>86%-83%</td><td>87%-85%</td>
                        </tr>
                        <tr>
                            <td>7-8</td><td>80%</td><td>80%-78%</td><td>83%-80%</td>
                        </tr>
                        <tr>
                            <td>9-10</td><td>75%</td><td>76%-75%</td><td>77%-75%</td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center">
                   Wenn Du also z.B. 5 Wiederholungen mit 50 Kg im Bankdrücken schaffst, dann kannst Du nach der RM-Methode[Ruhne (1992)] maximal 58,75 Kg drücken.
                   <br><br><b>eingesetzes Gewicht / prozentualer Wert aus Tabelle (ausgeübte Wiederholungen) * 100 % = maximales Gewicht</b>
                   <br><br>
                   100 % bzw. die maximale Leistung wird auch als 1RM dargestellt (1Repetition Maximums). Das optimale Trainingsgwicht kannst Du ermitteln, indem Du statt der 100 %, den prozentulen Wert aus der Tabelle für eine bestimmte Anzahl von Wiederholungen einträgst! Also z.B. 75 % für 10 Wiederholungen nach Ruhne (1992).
                    <br><br><b>eingesetzes Gewicht / prozentualer Wert aus Tabelle (ausgeübte Wiederholungen) * prozentualer Wert aus Tabelle(Ziel Wiederholungen) = optimales Gewicht</b>
                    <br><br>Die Berechnungen übernehmen wir natürlich für dich und geben dir damit die Möglichkeit, deine Steigerungen noch umfangreicher zu Tracken, damit Du noch präzisere Trainingspläne ausarbeiten kannst!
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- !!! Übung bearbeiten !!! -->
<form action="/userarea/exercise/{{$user_exercise->id}}/update" method="POST" class="form-inline" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="" value="{{$user_exercise->id}}" class="form-control">
    Trainingsgewicht:
    <div class="input-group">
        <input value="{{$user_exercise->train_value}}" type="number" name="train_value" class="form-control" min="0"} max="" step="" required="required" title="">
        <div class="input-group-addon">Kg</div>
    </div>
    RM Wert:
    <div class="input-group">
        <input value="{{$user_exercise->rm_value}}" type="number" name="rm_value" class="form-control" min="0"} max="" step="" required="required" title="">
        <div class="input-group-addon">%</div>
    </div>
    Maximales Trainingsgewicht
    <div class="input-group">
        <input value="<?php $max_train_weight = $user_exercise->train_value / $user_exercise->rm_value * 100; echo $max_train_weight; ?>" type="number" name="max_train_weight" class="form-control" min="0"} max="" step="" title="" disabled>
        <div class="input-group-addon">Kg</div>
    </div><br><br>

    <div class="text-center"><button type="submit" class="btn btn-primary">Speichern</button></div>
</form>
<br>

<!-- !!! Letze Änderungen / Tracking in Tabelle anzeigen !!! -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h2>Letzte Änderungen / Tracking</h2>
</div>
<table class="table table-hover">
    <thead>
        <tr>
            <th>Feld Name</th>
            <th>Alter Wert</th>
            <th>Neuer Wert</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_exercise->revisionHistory as $history)
        <tr>
            <td>{{ $history->fieldName() }}</td>
            <td>{{ $history->oldValue() }}</td>
            <td>{{ $history->newValue() }}</td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection
