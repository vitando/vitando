@extends('exercise.layout')

@section('layoutcontent')


<!-- !!! Übung anzeigen !!! -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h1>{{$exercise->title}}</h1>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h3>{{$exercise->description}}</h3>
    @if($check == false)
    <form action="/exercise/{{$exercise->id}}/add_to_user" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Favourite!</button>
    </div>
    </form>
    @endif
    @if($like == false)
    <form action="/exercise/{{$exercise->id}}/like" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Like!</button>
    </div>
    </form>
    @else
    <form action="/exercise/{{$exercise->id}}/like" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Dislike!</button>
    </div>
    </form>
    @endif
</div>

<!-- Kommentarfunktion -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <legend>Kommentare</legend>

    @foreach($notes as $note)
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->value}}

        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->created_at}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->users->name}}
        </div>
        
    @endforeach

</div>

<form action="/exercise/{{$exercise->id}}/note" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Kommentar hinzufügen</legend>
    <div class="form-group">
        <label for="">Kommentar</label>
        <input type="text" class="form-control" name="note" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>



<!-- !!! Übungen in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Übungen</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exercises as $oexercise)
        <tr>
            <td><a href="/exercise/{{$oexercise->id}}/show">{{$oexercise->title}}</a></td>
            <td>{{$oexercise->exercises_categories()->first()->title}}</td>
            <td>{{$oexercise->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
