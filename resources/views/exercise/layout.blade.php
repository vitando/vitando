@extends('master')

@section('content')
<br><br><br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading text-center"><b>Übungen</b></div>
                <div class="panel-body">
                @yield('layoutcontent')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
