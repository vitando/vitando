@extends('exercise.layout')

@section('layoutcontent')

<!-- !!! Übungen in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <legend class="text-center">Übungen</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exercises as $oexercise)
        <tr>
            <td>{{$oexercise->title}} <a href="/exercise/{{$oexercise->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            <td>{{$oexercise->exercises_categories()->first()->title}}</td>
            <td>{{$oexercise->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

<!-- !!! Übung bearbeiten !!! -->
<form action="/exercise/{{$exercise->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Übung bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$exercise->title}}" placeholder="{{$exercise->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
        <select name="exercises_categorie_id" type="number" class="form-control" required="required">
            <option value="{{$exercise->exercises_categorie_id}}" selected>{{$exercise->exercises_categories()->first()->title}}</option>
            @foreach($exercises_categories as $exercises_categorie)
                    <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
            @endforeach
        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$exercise->description}}</textarea>
    </div>                    
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
