@extends('exercise.layout')

@section('layoutcontent')
<!-- !!! Übungen in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($exercises as $oexercise)
        <tr>
            <td> <a href="/exercise/{{$oexercise->id}}/show">{{$oexercise->title}}</a>  </td>
            <td>{{$oexercise->exercises_categories()->first()->title}}</td>
            <td>{{$oexercise->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
