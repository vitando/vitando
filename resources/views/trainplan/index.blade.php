@extends('trainplan.layout')
@section('layoutcontent')
<!-- !!! Übungen in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($trainplans as $otrainplan)
        <tr>
            <td> <a href="/trainplan/{{$otrainplan->id}}/show">{{$otrainplan->title}}</a>  </td>
            <td>{{$otrainplan->trainplans_categories()->first()->title}}</td>
            <td>{{ str_limit($otrainplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
