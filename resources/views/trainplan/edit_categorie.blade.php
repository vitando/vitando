@extends('trainplan.layout')

@section('layoutcontent')

<!-- !!! Trainingspläne in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für Trainingspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($trainplans_categories as $otrainplans_categorie)
        <tr>
            <td>{{$otrainplans_categorie->title}} <a href="/trainplancategorie/{{$otrainplans_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $otrainplans_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$otrainplans_categorie->trainplans_categories()->first()->title}}</td>
            @endif
            <td>{{$otrainplans_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorie für Übung bearbeiten !!! -->

<form action="/trainplancategorie/{{$trainplans_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für Trainingspläne bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$trainplans_categorie->title}}" placeholder="{{$trainplans_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($trainplans_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($trainplans_categories as $atrainplans_categorie)
                    <option value="{{$atrainplans_categorie->id}}">{{$atrainplans_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($trainplans_categories as $atrainplans_categorie)
                    @if($atrainplans_categorie->id == $trainplans_categorie->scope)
                        <option value="{{$atrainplans_categorie->id}}" selected>{{$atrainplans_categorie->title}}</option>
                    @else
                        <option value="{{$atrainplans_categorie->id}}">{{$atrainplans_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$trainplans_categorie->description}}</textarea>
    </div>                    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
