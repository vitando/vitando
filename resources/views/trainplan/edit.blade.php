@extends('trainplan.layout')
@php ($old_day = $trainplan->trainplans_exercises()->first()->day)
@php ($trainplans_exercises = $trainplan->trainplans_exercises()->get())
@php ($trainplans_exercises = $trainplans_exercises->sortBy('day'))

@section('layoutcontent')

<!-- !!! Trainingspläne in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Deine Trainingspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($trainplans as $otrainplan)
        <tr>
            <td>{{$otrainplan->title}} <a href="/trainplan/{{$otrainplan->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            <td>{{$otrainplan->trainplans_categories()->first()->title}}</td>
            <td>{{ str_limit($otrainplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

<!-- !!! Trainingspläne bearbeiten !!! -->

<form action="/trainplan/{{$trainplan->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Trainingsplan bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$trainplan->title}}" placeholder="{{$trainplan->title}}">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
        <select name="trainplans_categorie_id" class="form-control" required="required">
            <option value="{{$trainplan->trainplans_categorie_id}}" selected>{{$trainplan->trainplans_categories()->first()->title}}</option>
            @foreach($trainplans_categories as $trainplans_categorie)
                    <option value="{{$trainplans_categorie->id}}">{{$trainplans_categorie->title}}</option>
            @endforeach
        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$trainplan->description}}</textarea>
    </div>                    
    
    <!-- Hier fängt der Tag an! -->

        
    @foreach($trainplans_exercises as $exercise)
        @if($loop->first)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$exercise->day}}" required>
                </div>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-plan-day"></button>
            </div>
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Übung</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">Sets</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">Reps</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">RM</label>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
        @endif
        @if($exercise->day != $old_day)
            <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus insert"></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                    <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$exercise->day}}" required>
                </div>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-plan-day" disabled></button>
            </div>
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Übung</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">Sets</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">Reps</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for="">RM</label>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
            <div class="row form-group plan-insert">
            <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="exercise" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($exercises_categories as $exercises_categorie)
                                <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="exercises[]" type="hidden" class="form-control new-day-id" value="{{$exercise->id}}">
                <input name="exercises[]" type="hidden" class="form-control change-day-h" value="{{$exercise->day}}">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select name="exercises[]" class="form-control insert-with-categorie" readonly><option value="{{ $exercise->exercise()->first()->id }}">{{ $exercise->exercise()->first()->title }}</option></select>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->sets }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->reps }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->rm_value }}">
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <input name="exercises[]" type="text" class="form-control" value="{{ $exercise->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert" disabled></button>
                </div>
            </div>
            @php ($old_day = $exercise->day)
        @else        
            <div class="row form-group plan-insert">
                <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="exercise" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($exercises_categories as $exercises_categorie)
                                <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="exercises[]" type="hidden" class="form-control new-day-id" value="{{$exercise->id}}">
                <input name="exercises[]" type="hidden" class="form-control change-day-h" value="{{$exercise->day}}">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select name="exercises[]" class="form-control insert-with-categorie" readonly><option value="{{ $exercise->exercise()->first()->id }}">{{ $exercise->exercise()->first()->title }}</option></select>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->sets }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->reps }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <input name="exercises[]" type="number" class="form-control" value="{{ $exercise->rm_value }}">
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <input name="exercises[]" type="text" class="form-control" value="{{ $exercise->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert"></button>
                </div>
            </div>
            @php ($old_day = $exercise->day)
        @endif
    @endforeach

        <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus insert"></button>
    </div> 
    <button type="submit" class="btn btn-primary">Submit</button>


</form>


@endsection
