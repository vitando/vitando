@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Trainingsplan personalisieren</h4>
@endsection
@php ($old_day = 1)

<?php $timestamp = time();
$time = date("m/d/Y", $timestamp); ?>

@section('layoutcontent')

    <!-- Tabelle für Ernährungsplan Tage mit dragable -->
  <div class="table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Übungen</th>
          <th>Kategorie</th>
        </tr>
      </thead>
      <tbody>
      @foreach($days as $key => $day)
        <tr>
          <td><span data-id="{{ $trainplan->id }}" data-type="tpday" data-name="{{ $key }}" class="btn btn-success draggable get-tpexercise">{{ $key }}</span></td>
          <td>{{ str_limit($day['title'], $limit = 50, $end = '...') }}</td>
          <td>{{ $trainplan->trainplans_categories->title }} </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
       
       <!-- Rezepte bei klick anzeigen -->
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel panel-info">
  <h3 id="day-title" class="text-center"></h3>
      <table class="table table-hover">
          <thead>
              <tr>
                <th>Übung Titel</th>
                <th>Sets</th>
                <th>Reps</th>
                <th>RM/Gewicht</th>
              </tr>
          </thead>
          <tbody class="insert-tpday">
              <tr>
                <th></th>
                <th></th>
                <th class="text-center">Oben klicken um Tag zu laden!</th>
                <th></th>
                <th></th>
              </tr>
          </tbody>
      </table>
  </div> 

  <!-- Kalender speichern Button! -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
  <button type="button" class="btn btn-success save-clndr">Kalender Speichern!</button>
  <br><br>
</div>  
      <!-- Kalenderfunktion -->
  <script type="text/template" id="swag">
    <div class='clndr-controls'>
      <div class="row">
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-md-offset-2">
          <span class='clndr-control-button'>
              <span class='clndr-previous-button glyphicon glyphicon-arrow-left'></span>
          </span>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <span class='month'><%= month %> <%= year %></span>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
          <span class='clndr-control-button rightalign'>
              <span class='clndr-next-button glyphicon glyphicon-arrow-right'></span>
          </span>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
        <table class='cal-body' border='0' cellspacing='0' cellpadding='0'>
          <thead>
              <tr class='header-days'>
              <% for(var i = 0; i < daysOfTheWeek.length; i++) { %>
                  <td class='header-day'><h4><%= daysOfTheWeek[i] %></h4></td>
              <% } %>
              </tr>
          </thead>
          <tbody>
          <% for(var i = 0; i < numberOfRows; i++){ %>
              <tr class="cal-day">
              <% for(var j = 0; j < 7; j++){ %>
              <% var d = j + i * 7; %>
              <% var time = days[d].date + 86400000; %>
              <% var day = new Date(time).toISOString(); %>
              <% var date = day.substring(0, 10); %>

                  <td id="<%= date %>" class='droppable <%= days[d].classes %>'>
                      <div class='day-content'><%= days[d].day %></div>
                        <% _.each(eventsThisMonth, function(event) { %>
                        <% if ( event.date === date ) { %>
                          <div class="event-item">
                          <span data-id="<%= event.id %>" data-type="<%= event.type %>" data-name="<%= event.title %>" class="btn btn-success get-fprecipe"><%= event.title %></span>
                          </div>
                        <% } %>
                        <% }); %>
                  </td>
              <% } %>
              </tr>
          <% } %>
          </tbody>
        </table>
    </div>        
    </div>
  </script>
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <div id="cal1"></div>
  </div>
</div>

@endsection


