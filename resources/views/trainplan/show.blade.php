@extends('trainplan.layout')

@section('layoutcontent')


<!-- !!! Übung anzeigen !!! -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h1>{{$trainplan->title}}</h1>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h3>{{$trainplan->description}}</h3>
    @if($check == false)
    <form action="/trainplan/{{$trainplan->id}}/add_to_user" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Favourite!</button>
    </div>
    </form>
    @endif
</div>

<!-- Kommentarfunktion -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <legend>Kommentare</legend>

    @foreach($notes as $note)
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->value}}

        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->created_at}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->users->name}}
        </div>
        
    @endforeach

</div>

<form action="/trainplan/{{$trainplan->id}}/note" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Kommentar hinzufügen</legend>
    <div class="form-group">
        <label for="">Kommentar</label>
        <input type="text" class="form-control" name="note" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>



<!-- !!! Übungen in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Trainingspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($trainplans as $otrainplan)
        <tr>
            <td><a href="/trainplan/{{$otrainplan->id}}/show">{{$otrainplan->title}}</a></td>
            <td>{{$otrainplan->trainplans_categories()->first()->title}}</td>
            <td>{{ str_limit($otrainplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
