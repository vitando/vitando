@extends('blog.layout')

@section('layoutcontent')

<!-- !!! Blogs in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Deine Blogs</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($blogs as $oblog)
        <tr>
            <td>{{$oblog->title}} <a href="/blog/{{$oblog->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            <td>{{$oblog->blogs_categories()->first()->title}}</td>
            <td>{{$oblog->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Blogs bearbeiten !!! -->

<form action="/blog/{{$blog->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Blogs bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$blog->title}}" placeholder="{{$blog->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
        <select name="blogs_categorie_id" type="number" class="form-control" required="required">
            <option value="{{$blog->blogs_categorie_id}}" selected>{{$blog->blogs_categories()->first()->title}}</option>
            @foreach($blogs_categories as $blogs_categorie)
                    <option value="{{$blogs_categorie->id}}">{{$blogs_categorie->title}}</option>
            @endforeach
        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Kurzbeschreibung</label>
        <textarea name="description" class="form-control" rows="3" required="required">{{$blog->description}}</textarea>
    </div>                      
    <div class="form-group col-sm-12">
        <label for="">Eintrag</label>
        <textarea name="text" class="form-control" rows="10" required="required">{{$blog->text}}</textarea>
    </div>                    
    
    

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
