@extends('blog.layout')
@section('layoutcontent')
<!-- !!! Blogs in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($blogs as $oblog)
        <tr>
            <td> <a href="/blog/{{$oblog->id}}/show">{{$oblog->title}}</a>  </td>
            <td>{{$oblog->blogs_categories()->first()->title}}</td>
            <td>{{$oblog->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>
@endsection
