@extends('blog.layout')
@section('layoutcontent')

<!-- !!! Blog anzeigen !!! -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h1>{{$blog->title}}</h1>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h3>{{$blog->description}}</h3>
    
    @if($check == false)
    <form action="/blog/{{$blog->id}}/add_to_user" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Favourite!</button>
    </div>
    </form>
    @endif
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {{$blog->text}}<br><br>

</div>

<!-- Kommentarfunktion -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <legend>Kommentare</legend>
    @foreach($notes as $note)
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->value}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->timestamps}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->users->name}}
        </div>
    @endforeach
</div>

<form action="/blog/{{$blog->id}}/note" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Kommentar hinzufügen</legend>
    <div class="form-group">
        <label for="">Kommentar</label>
        <input type="text" class="form-control" name="note" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<!-- !!! Blogs in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <legend class="text-center">Blogs</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($blogs as $oblog)
        <tr>
            <td><a href="/blog/{{$oblog->id}}/show">{{$oblog->title}}</a></td>
            <td>{{$oblog->blogs_categories()->first()->title}}</td>
            <td>{{$oblog->description}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
