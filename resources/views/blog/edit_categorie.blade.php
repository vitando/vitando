@extends('blog.layout')

@section('layoutcontent')

<!-- !!! Kategorien in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für Blogs</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($blogs_categories as $oblogs_categorie)
        <tr>
            <td>{{$oblogs_categorie->title}} <a href="/blogcategorie/{{$oblogs_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $oblogs_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$oblogs_categorie->blogs_categories()->first()->title}}</td>
            @endif
            <td>{{$oblogs_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorie für Blogs bearbeiten !!! -->

<form action="/blogcategorie/{{$blogs_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für Blogs bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$blogs_categorie->title}}" placeholder="{{$blogs_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($blogs_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($blogs_categories as $ablogs_categorie)
                    <option value="{{$ablogs_categorie->id}}">{{$ablogs_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($blogs_categories as $ablogs_categorie)
                    @if($ablogs_categorie->id == $blogs_categorie->scope)
                        <option value="{{$ablogs_categorie->id}}" selected>{{$ablogs_categorie->title}}</option>
                    @else
                        <option value="{{$ablogs_categorie->id}}">{{$ablogs_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$blogs_categorie->description}}</textarea>
    </div>                    
    
    

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
