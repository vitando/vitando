@extends('userarea.layout')
@section('layoutcontenttitle')
    <h4>Dein Netzwerk</h4>
@endsection
@section('layoutcontent')
<span class="btn btn-large btn-block btn-success disabled" role="button">Offene Freundschaftsanfragen</span><br>

@foreach($befriends as $friend)
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <span class="btn btn-large btn-block btn-success disabled">{{$friend->nick}}</span><br>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <span class="btn btn-large btn-block btn-success" role="button">Akzeptieren</span><br>
    </div>
@endforeach

@endsection
