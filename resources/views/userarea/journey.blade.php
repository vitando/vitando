@extends('userarea.layout')

@section('layoutcontent')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   <h1 class="text-center">Alles in einem Programm für einen fairen Preis!</h1> 

<form action="" method="POST" role="form">
  <legend><br><br>Jetzt registrieren!</legend>

  <div class="form-group">
    <label for="">Name</label>
    <input type="text" class="form-control" id="" >
  </div>
  <div class="form-group">
    <label for="">Straße</label>
    <input type="text" class="form-control" id="" >
  </div>
  <div class="form-group">
    <label for="">Hausnummer</label>
    <input type="text" class="form-control" id="" >
  </div>
  <div class="form-group">
    <label for="">Ort</label>
    <input type="text" class="form-control" id="" >
  </div>
  <div class="form-group">
    <label for="">Postleitzahl</label>
    <input type="text" class="form-control" id="" >
  </div>

</form>

</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Beginne deine Reise!</button>
</div>


<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="js-title-step"></h4>
      </div>
      <div class="modal-body">
        <div class="row hide" data-step="1" data-title="Herzlichen Glückwunsch!">
          <div class="jumbotron">
            <h3 class="text-center">Du bist jetzt Vitando-Mitglied der Stufe 1 (Bronze). <br> <br>Du kannst jetzt Blogs und Rezepte kommentieren und teilen... Trage jetzt noch schnell einige Dinge nach und schalte weitere Features frei!</h3>   
          </div>
        </div>

        <div class="row hide" data-step="2" data-title="Was willst Du erreichen?!">
          <div class="jumbotron">
            <h4 class="text-center">Hier kannst Du bestimmen, was Du mit unserer Unterstützung erreichen willst. Zudem können wir mit diesen Angaben deinen persönlichen Fortschritt besser auswerten!</h4>
              <form action="" method="POST" role="form">
                  <div class="form-group">
                      <label for="">Ziel bestimmen!</label>
                      <select name="" id="input" class="form-control" required="required">
                          <option value="">Abnehmen</option>
                          <option value="">Zunehmen</option>
                          <option value="">Muskelaufbau</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label for="">Ziel definieren! z.B. Angabe des Zielgewichts</label>
                      <input type="text" class="form-control" id="" placeholder="">
                  </div>

          </div>
        </div>
        <div class="row hide" data-step="3" data-title="Angaben zur Berechnung Ihres Grundumsatzes.">
          <div class="jumbotron">
            <h4 class="text-center">Für eine persönliche Beratung werden natürlich auch persönliche Daten benötigt! Sei Gewiß, dass wir mit diesen Daten stes sorgfältig und höhst diskret umgehen werden!</h4>
                  <div class="form-group">
                      <label for="">Geschlecht</label>
                      <select name="" id="input" class="form-control" required="required">
                          <option value="0">Weiblich</option>
                          <option value="1">Männlich</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label for="">Alter in Jahren</label>
                      <input type="number" class="form-control" id="" placeholder="">
                  </div>
                  <div class="form-group">
                      <label for="">Gewicht in Kg</label>
                      <input type="number" class="form-control" step="0.01" min="0" id="">
                  </div>
                  <div class="form-group">
                      <label for="">Größe in cm</label>
                      <input type="number" class="form-control" min="0" id="">
                  </div>
                  <div class="form-group">
                      <label for="">circa Pal-Wert</label>
                      <input type="number" class="form-control" min="0.85" step="0.01" id="" placeholder="">
                  </div>
              </form>
          </div>
        </div>
        <div class="row hide" data-step="4" data-title="Die ersten erfolgreichen Schritte!">
          <div class="jumbotron">
            <h4 class="text-center">Wir danken dir schon jetzt für deine Registrierung und wünschen dir viel Spaß bei vitando!<br>Lass gut gehen!</h4>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button>
        <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
        <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
      </div>
    </div>
  </div>
</div>


@endsection
