@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Tools & Statistiken</h4>
@endsection
@section('layoutcontent')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h4>Ziel ändern</h4>
</div>
<form action="/userarea/aim/update" method="POST" class="form" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label class="" for="">Ziel</label>
                <select name="aim_id" id="input" class="form-control" required="required">
                    @foreach($aims as $oaim)
                        @if($oaim->id == $aim->aim->id)
                        <option value="{{$oaim->id}}" selected>{{$oaim->title}}</option>
                        @else
                        <option value="{{$oaim->id}}">{{$oaim->title}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
                <label class="" for="">Gewichtsveränderung</label>
                <input type="number" name="weight" class="form-control" value="{{$aim->weight}}" min="-255"} max="255" step="1" required="required">
            </div>
        </div>
        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <div class="form-group">
                <label class="" for="">Notiz</label>
                <input type="text" name="note" class="form-control" value="{{$aim->note}}" required="required">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="" for=""><br></label>
            <button type="submit" class="btn btn-primary">Speichern</button>
        </div>
    </div>
</form>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h4>Dein ermittelter Energiebedarf</h4>
</div>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Grundumstz</th>
            <th>PAL Fakrtor</th>
            <th>Leistungsumsatz</th>
            <th>Carbs</th>
            <th>Proteine</th>
            <th>Fett</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_pals as $pal)
        <tr>
            <td>{{$pal->title}}</td>
            <td>{{$pal->energy}}</td>
            <td>{{$pal->pal_day}}</td>
            <td>{{$pal->energy_total}}</td>
            <td>{{$energy_aim[$pal->id]['carbs']}}</td>
            <td>{{$energy_aim[$pal->id]['protein']}}</td>
            <td>{{$energy_aim[$pal->id]['fat']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
