@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Deine Rezepte</h4>
@endsection
@section('layoutcontent')

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>tit
title</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_recipes as $recipe)
        <tr>
            <td><a href="/recipe/{{$recipe->recipe->id}}/show">{{$recipe->recipe->title}}</a></td>
            <td>{{$recipe->recipe->recipes_categories()->first()->title}}</td>
            <td>{{$recipe->recipe->description}}</td>
        </tr>
        @endforeach
    </tbody>
</table>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a href="/recipe/index">Zu allen Rezepten</a>
</div>
@endsection
