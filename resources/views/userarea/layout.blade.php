@extends('master')

@section('content')
<br><br><br><br><br>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><a href="/userarea">Userarea</a></h3>
                </div>
                <div class="panel-body text-center">
                    <a href="/userarea/trainplan" class="col-xs-2 btn btn-primary" role="button">Deine Träningspläne</a>
                    <a href="/userarea/foodplan" class="col-xs-2 btn btn-primary" role="button">Deine Ernährungspläne</a>
                    <a href="/userarea/exercise" class="col-xs-2 btn btn-primary" role="button">Deine Übungen</a>
                    <a href="/userarea/recipe" class="col-xs-2 btn btn-primary" role="button">Deine Rezepte</a>
                    <a href="/userarea/blog" class="col-xs-2 btn btn-primary" role="button">Deine Blogs</a>
                    <a href="/userarea/edit" class="col-xs-2 btn btn-primary" role="button">Deine Daten</a>
                    <!-- <a href="/userarea/friendships" class="col-xs-2 btn btn-primary" role="button">Freunde</a> -->
                    <a href="/userarea/calendar" class="col-xs-2 btn btn-primary" role="button">Kalender</a>
                    @if(!empty($aim->id))
                        <a href="/userarea/calc" class="col-xs-2 btn btn-primary" role="button">Tools und Statistiken</a>
                        <a href="/userarea/pal" class="col-xs-2 btn btn-primary" role="button">Energiebedarf</a>
                    @else
                        <a href="/userarea/aim" class="col-xs-2 btn btn-primary" role="button">Ziel bestimmen</a>
                    @endif
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading text-center">@yield('layoutcontenttitle')</div>
                <div class="panel-body">
                @yield('layoutcontent')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
                @yield('script')
@endsection
