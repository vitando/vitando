@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Deine Ernährungspläne</h4>
@endsection

@section('layoutcontent')
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
            <th>Personalisieren</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_foodplans as $foodplan)
        <tr>
            <td><a href="/foodplan/{{$foodplan->foodplan->id}}/show">{{$foodplan->foodplan->title}}</a></td>
            <td>{{$foodplan->foodplan->foodplans_categories()->first()->title}}</td>
            <td>{{ str_limit($foodplan->foodplan->description, $limit = 100, $end = '...') }}</td>
            <td>
                @if(! empty($aim->id))
                    <a href="/foodplan/{{$foodplan->foodplan->id}}/personal" class="glyphicon glyphicon-asterisk"></a></td>
                @else
                    Ziel <a href="/userarea/aim">bestimmen!</a></td>
                @endif
        </tr>
            
        @endforeach

    </tbody>
</table>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a href="/foodplan/index">Zu allen Ernährungsplänen</a>
</div>


@endsection
