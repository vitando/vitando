<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel">Trainingplan: {{ $trainplan->title }}</h4>
</div>
<div class="modal-body">
@foreach($days as $key => $day)
	@if($day == 'name')
		<span class="btn btn-success active">{{ $day }}</span>
	@else
		<span class="btn btn-success">{{ $day }}</span>
	@endif
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Übung</th>
					<th>Sets</th>
					<th>Reps</th>
					<th>Notiz</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trainplan->trainplans_exercises->where('day', $day) as $exercise)
						<tr>
							<td>{{ $exercise->exercise->title }}</td>
							<td>{{ $exercise->sets }}</td>
							<td>{{ $exercise->reps }}</td>
							<td>{{ $exercise->description }}</td>
						</tr>
				@endforeach

			</tbody>
		</table>
	</div>
@endforeach
