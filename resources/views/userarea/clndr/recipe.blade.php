<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel">Rezept: {{ $recipe->recipe->title }}</h4>
</div>
<div class="modal-body">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Zutat</th>
					<th>Menge</th>
					<th>Notiz</th>
				</tr>
			</thead>
			<tbody>
				@foreach($recipe->recipe->foods as $recipe)
						<tr>
							<td>{{ $recipe->foods->name }}</td>
							<td>{{ $recipe->amount }}</td>
							<td>{{ $recipe->description }}</td>
						</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
