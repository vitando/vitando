<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel">Ernährungsplan: {{ $foodplan->title }}</h4>
</div>
<div class="modal-body">
@foreach($days as $key => $day)
	@if($day == 'name')
		<span class="btn btn-success active">{{ $day }}</span>
	@else
		<span class="btn btn-success">{{ $day }}</span>
	@endif
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Rezept</th>
					<th>Menge</th>
					<th>Notiz</th>
				</tr>
			</thead>
			<tbody>
				@foreach($foodplan->foodplans_recipes->where('day', $day) as $recipe)
						<tr>
							<td>{{ $recipe->recipe->title }}</td>
							<td>{{ $recipe->amount }}</td>
							<td>{{ $recipe->description }}</td>
						</tr>
				@endforeach

			</tbody>
		</table>
	</div>
@endforeach
