@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Deine Trainingspläne</h4>
@endsection

@section('layoutcontent')
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>tit
title</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
            <th>Personalisieren</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_trainplans as $trainplan)
        <tr>
            <td><a href="/trainplan/{{$trainplan->trainplan->id}}/show">{{$trainplan->trainplan->title}}</a></td>
            <td>{{$trainplan->trainplan->trainplans_categories()->first()->title}}</td>
            <td>{{ str_limit($trainplan->trainplan->description, $limit = 100, $end = '...') }}</td>
            <td><a href="/trainplan/{{$trainplan->trainplan->id}}/personal" class="glyphicon glyphicon-asterisk"></a></td>
        </tr>
            
        @endforeach

    </tbody>
</table>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a href="/trainplan/index">Zu allen Traingplänen</a>
</div>


@endsection
