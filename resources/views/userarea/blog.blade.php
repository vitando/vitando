@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Deine Blogs</h4>
@endsection

@section('layoutcontent')
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_blogs as $blog)
        <tr>
            <td><a href="/blog/{{$blog->blog->id}}/show">{{$blog->blog->title}}</a></td>
            <td>{{$blog->blog->blogs_categories()->first()->title}}</td>
            <td>{{$blog->blog->description}}</td>
        </tr>
        @endforeach
    </tbody>
</table>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a href="/blog/index">Zu allen Blogs</a>
</div>
@endsection
