@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Deine Übungen</h4>
@endsection
@section('layoutcontent')
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
            <th>Trainingsgewicht</th>
            <th>Maximales Gewicht</th>
            <th>Bearbeiten/Tracking</th>
        </tr>
    </thead>
    <tbody>
        @foreach($user_exercises as $exercise)
        <tr>
            <td><a href="/exercise/{{$exercise->exercise->id}}/show">{{$exercise->exercise->title}}</a></td>
            <td>{{$exercise->exercise->exercises_categories()->first()->title}}</td>
            <td>{{$exercise->exercise->description}}</td>
            <td>{{$exercise->train_value}}</td>
            <?php $max_train_weight = $exercise->train_value / $exercise->rm_value * 100; ?>
            <td>{{ $max_train_weight }}</td>
            <td><a href="/userarea/exercise/{{$exercise->id}}/edit" class="glyphicon glyphicon-asterisk"></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <a href="/exercise/index">Zu allen Übungen</a>
</div>
@endsection
