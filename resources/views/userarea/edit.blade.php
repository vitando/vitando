@extends('userarea.layout')
@section('layoutcontenttitle')
    <h4>Deine Daten</h4>
@endsection
@section('layoutcontent')

<!-- Daten anzeigen -->
<form action="/userarea/edit/{{ $user_addr->id }}" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <input name="type" type="hidden" value="{{ $user_addr->type }}">
        <div class="form-group">
            <label for="">Titel</label>
            <input name="title" type="text" class="form-control" value="{{ $user_addr->title }}">
        </div>
        <div class="form-group">
            <label for="">Vorname</label>
            <input name="vname" type="text" class="form-control" value="{{ $user_addr->vname }}">
        </div>
        <div class="form-group">
            <label for="">Nachname</label>
            <input name="nname" type="text" class="form-control" value="{{ $user_addr->nname }}">
        </div>
        <div class="form-group">
            <label for="">E-Mail</label>
            <input name="email" type="text" class="form-control" value="{{ $user->email }}">
        </div>
        <div class="form-group">
            <label for="">Gewicht</label>
            <input name="weight" type="text" class="form-control" value="{{ $user->weight }}">
        </div>
        <div class="form-group">
            <label for="">Größe</label>
            <input name="size" type="text" class="form-control" value="{{ $user->size }}">
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="">Geburtsdatum</label>
            <input type="date" name="bday" class="form-control" value="{{ $user->bday }}">
        </div>
        <div class="form-group">
            <label for="">Straße</label>
            <input name="street" type="text" class="form-control" value="{{ $user_addr->street }}">
        </div>
        <div class="form-group">
            <label for="">Hausnummer</label>
            <input name="street_number" type="text" class="form-control" value="{{ $user_addr->street_number }}">
        </div>
        <div class="form-group">
            <label for="">Straßenzusatz</label>
            <input name="street_add" type="text" class="form-control" value="{{ $user_addr->street_add }}">
        </div>
        <div class="form-group">
            <label for="">PLZ</label>
            <input name="zip_code" type="text" class="form-control" value="{{ $user_addr->zip_code }}">
        </div>
        <div class="form-group">
            <label for="">Ort</label>
            <input name="town" type="text" class="form-control" value="{{ $user_addr->town }}">
        </div>

    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-primary">Speichern</button><br><br>
    </div>
</form>

@if(!empty($user->instatoken))
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <span class="btn btn-large btn-block btn-success disabled" role="button">Mit Instagramm verbunden!</span><br>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <div id="insta"></div>
<br>
</div>
@section('script')
    <script>
        var items = '';
        $.ajax({
          method: "GET",
          url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token={{ $user->instatoken }}&callback=?',
          async: false,
          dataType: "jsonp",
          success: function (data) {
            $.each(data.data, function(id, data) {
                items += '<img src="' + data.images.low_resolution.url + '" class="img" alt="Image">';
            });
            $( "#insta" ).append(items);
          }
        });
    </script>
@endsection

@else
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <legend></legend>
    <a class="btn btn-success" href="https://api.instagram.com/oauth/authorize/?client_id=60d2b9faef3a46ce8af62b85517dcf58&redirect_uri=http://vitando.dev/userarea/edit&response_type=token&scope=public_content+follower_list">Mit Instagramm verbinden</a><br><br>
    <legend></legend>
</div>
@section('script')
    <script>
    var token = (window.location.hash);
    if (!token == null || !token==''){
        var split = token.split("n=");
        var access = split[1];
        $.ajax({
          method: "POST",
          url: '/userarea/insta/' + access,
          data: {"_token": "{{ csrf_token() }}"},
          async: false,
          success: function () {
            window.location.replace("/userarea/edit");
          }
        });
    }
    </script>
@endsection
@endif

<!-- Kategorien für Trainersuche -->

@if(!empty($cats))
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
<legend>Kategorien für Trainersuche bestimmen</legend>
    <ul class="list-group">

        @foreach($cats as $cat)
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <li class="list-group-item">{{$cat->title}}</li>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                @if(empty($cat->value))
                <li class="list-group-item list-group-item-warning">Noch nicht aktiviert!</li>
                @else
                <li class="list-group-item">{{$cat->value}}</li>
                @endif
            </div>
        @endforeach
    
    </ul>
</div>
@endif



<!-- Modal Adresse anlegen -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <br><br><legend></legend>
    <a class="btn btn-info" data-toggle="modal" href='#modal-id'>Neue Adresse anlegen</a><br><br>
    <legend></legend>
</div>

<div class="modal fade" id="modal-id">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Neue Adresse</h4>
            </div>
            <div class="modal-body">
                <form action="/userarea/edit/create" method="POST" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="">Typ</label>
                        <select name="type" id="inputType" class="form-control">
                            <option value="ship">Lieferadresse</option>
                            <option value="invoice">Rechnungsadresse</option>
                            <option value="both">Liefer- und Rechnungsadresse</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Titel</label>
                        <input name="title" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Vorname</label>
                        <input name="vname" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Nachname</label>
                        <input name="nname" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">E-Mail</label>
                        <input name="email" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Straße</label>
                        <input name="street" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Hausnummer</label>
                        <input name="street_number" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Straßenzusatz</label>
                        <input name="street_add" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">PLZ</label>
                        <input name="zip_code" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Stadt</label>
                        <input name="town" type="text" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                <button type="submit" class="btn btn-primary">Speichern</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Alle Adressen -->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <legend><h3>Deine Adressen</h3></legend>
</div>
@foreach($user->addrs as $addr)
@if($addr->type == 'default')
@else
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <form action="/userarea/edit/{{$addr->id}}" method="POST" role="form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="">Typ</label>
            <select name="type" id="inputType" class="form-control">
            @if($addr->type == 'ship')
                <option value="ship" selected>Lieferadresse</option>
                <option value="invoice">Rechnungsadresse</option>
                <option value="both">Liefer- und Rechnungsadresse</option>
            @else
                @if($addr->type == 'invoice')
                    <option value="ship">Lieferadresse</option>
                    <option value="invoice" selected>Rechnungsadresse</option>
                    <option value="both">Liefer- und Rechnungsadresse</option>
                @else
                    <option value="ship">Lieferadresse</option>
                    <option value="invoice">Rechnungsadresse</option>
                    <option value="both" selected>Liefer- und Rechnungsadresse</option>
                @endif
            @endif
            </select>
        </div>

        <div class="form-group">
            <label for="">Titel</label>
            <input name="title" type="text" class="form-control" value="{{ $addr->title }}">
        </div>

        <div class="form-group">
            <label for="">Vorname</label>
            <input name="vname" type="text" class="form-control" value="{{ $addr->vname }}">
        </div>

        <div class="form-group">
            <label for="">Nachname</label>
            <input name="nname" type="text" class="form-control" value="{{ $addr->nname }}">
        </div>

        <div class="form-group">
            <label for="">E-Mail</label>
            <input name="email" type="text" class="form-control" value="{{ $addr->email }}">
        </div>

        <div class="form-group">
            <label for="">Straße</label>
            <input name="street" type="text" class="form-control" value="{{ $addr->street }}">
        </div>

        <div class="form-group">
            <label for="">Hausnummer</label>
            <input name="street_number" type="text" class="form-control" value="{{ $addr->street_number }}">
        </div>

        <div class="form-group">
            <label for="">Straßenzusatz</label>
            <input name="street_add" type="text" class="form-control" value="{{ $addr->street_add }}">
        </div>

        <div class="form-group">
            <label for="">PLZ</label>
            <input name="zip_code" type="text" class="form-control" value="{{ $addr->zip_code }}">
        </div>

        <div class="form-group">
            <label for="">Stadt</label>
            <input name="town" type="text" class="form-control" value="{{ $addr->town }}">
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Speichern</button>
        </div>
    </form>
</div>
@endif
@endforeach
@endsection
