@extends('userarea.layout')
@section('layoutcontenttitle')
<h4>Tools & Statistiken</h4>
@endsection
@section('layoutcontent')
<form action="/userarea/aim/create" method="POST" class="form" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                <label class="" for="">Ziel</label>
                <select name="aim_id" id="input" class="form-control" required="required">
                    @foreach($aims as $oaim)
                    <option value="{{$oaim->id}}">{{$oaim->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
                <label class="" for="">Gewichtsveränderung</label>
                <input type="number" name="weight" class="form-control" value="" min="-255"} max="255" step="1" required="required">
            </div>
        </div>
        <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
            <div class="form-group">
                <label class="" for="">Notiz</label>
                <input type="text" name="note" class="form-control" required="required">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label class="" for=""><br></label>
            <button type="submit" class="btn btn-primary">Speichern</button>
        </div>
    </div>
</form>

@endsection
