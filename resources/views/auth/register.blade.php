@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">Registrieren</div>
                <div class="panel-body">
                    <form class="form-horizontal journey" role="form" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="type" class="col-md-4 control-label">Titel</label>
                            <div class="col-md-2">
                                <select name="type" class="form-control" required="">
                                    <option value="trainer">Trainer</option>
                                    <option value="user" selected>User</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-md-4 control-label">Titel</label>
                            <div class="col-md-2">
                                <input name="title" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nickname</label>
                            <div class="col-md-5">
                                <input name="name" id="name" type="text" class="form-control"  value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Vorname</label>
                            <div class="col-md-5">
                                <input name="vname" type="text" class="form-control" value="{{ old('vname') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname" class="col-md-4 control-label">Nachname</label>
                            <div class="col-md-5">
                                <input name="nname" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Adresse</label>
                            <div class="col-md-5">
                                <input name="email" id="email" type="email" class="form-control" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Passwort</label>
                            <div class="col-md-5">
                                <input name="password" id="password" type="password" class="form-control" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Passwort bestätigen</label>
                            <div class="col-md-5">
                                <input name="password_confirmation" id="password-confirm" type="password" class="form-control" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname" class="col-md-4 control-label">PLZ</label>
                            <div class="col-md-1">
                                <input name="zip_code" type="text" class="form-control" required>
                            </div>
                            <label for="nname" class="col-md-1 control-label">Stadt</label>
                            <div class="col-md-3">
                                <input name="town" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname" class="col-md-4 control-label">Straße</label>
                            <div class="col-md-2">
                                <input name="street" type="text" class="form-control" required>
                            </div>
                            <label for="nname" class="col-md-1 control-label">Hausnummer</label>
                            <div class="col-md-2">
                                <input name="street_number" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Registrieren
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="modal fade" id="journey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="js-title-step"></h4>
                          </div>
                          <div class="modal-body">
                            <div class="row hide" data-step="1" data-title="Herzlichen Glückwunsch!">
                              <div class="jumbotron">
                                <h3 class="text-center">Du bist jetzt Vitando-Mitglied der Stufe 1 (Bronze). <br> <br>Du kannst jetzt Blogs und Rezepte kommentieren und teilen... Trage jetzt noch schnell einige Dinge nach und schalte weitere Features frei!</h3>   
                              </div>
                            </div>

                            <div class="row hide" data-step="2" data-title="Was willst Du erreichen?!">
                              <div class="jumbotron">
                                <h4 class="text-center">Hier kannst Du bestimmen, was Du mit unserer Unterstützung erreichen willst. Zudem können wir mit diesen Angaben deinen persönlichen Fortschritt besser auswerten!</h4>
                                  <form role="form" id="journey-modal">
                                      <div class="form-group">
                                          <label for="">Ziel bestimmen!</label>
                                          <select name="aim_id" id="input" class="form-control" required="required">
                                              @foreach($aims as $aim)
                                              <option value="{{$aim->id}}">{{$aim->title}}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label class="" for="">Gewichtsveränderung</label>
                                          <input type="number" name="aim_weight" class="form-control" value="" min="-255"} max="255" step="1" required="required">
                                      </div>
                                      <div class="form-group">
                                          <label for="">Notiz</label>
                                          <input type="text" name="aim_note" class="form-control" required="required">
                                      </div>

                              </div>
                            </div>
                            <div class="row hide" data-step="3" data-title="Angaben zur Berechnung Ihres Grundumsatzes.">
                              <div class="jumbotron">
                                <h4 class="text-center">Für eine persönliche Beratung werden natürlich auch persönliche Daten benötigt! Sei Gewiß, dass wir mit diesen Daten stes sorgfältig und höhst diskret umgehen werden!</h4>
                                      <div class="form-group">
                                          <label for="">Geschlecht</label>
                                          <select name="gender" id="input" class="form-control" required="required">
                                              <option value="1">Weiblich</option>
                                              <option value="0">Männlich</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label for="">Geburtsdatum</label>
                                          <input name="bday" type="date" class="form-control">
                                      </div>
                                      <div class="form-group">
                                          <label for="">Gewicht in Kg</label>
                                          <input name="weight" type="number" class="form-control" step="0.01" min="0">
                                      </div>
                                      <div class="form-group">
                                          <label for="">Größe in cm</label>
                                          <input name="size" type="number" class="form-control" min="0">
                                      </div>
                                  </form>
                              </div>
                            </div>
                            <div class="row hide" data-step="4" data-title="Die ersten erfolgreichen Schritte!">
                              <div class="jumbotron">
                                <h4 class="text-center">Wir danken dir schon jetzt für deine Registrierung und wünschen dir viel Spaß bei vitando!<br>Lass gut gehen!</h4>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="{{ url('/') }}"><button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button></a>
                            <button type="button" class="btn btn-warning js-btn-step" data-orientation="previous"></button>
                            <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
