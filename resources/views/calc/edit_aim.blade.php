@extends('trainerarea.layout')
@section('layoutcontent')

<!-- !!! Ziele in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Ziele</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Beschreibung</th>
            <th>Fett</th>
            <th>Kohlenhydrate</th>
            <th>Proteine</th>
        </tr>
    </thead>
    <tbody>
        @foreach($aims as $oaim)
        <tr>
            <td>{{$oaim->title}} <a href="/trainerarea/aim/{{$oaim->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            <td>{{$oaim->description}}</td>
            <td>{{$oaim->fat}}</td>
            <td>{{$oaim->carbs}}</td>
            <td>{{$oaim->protein}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>
<br>
<!-- !!! Ziel erstellen !!! -->

<form action="/trainerarea/aim/{{$aim->id}}/update" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Ziel erstellen</legend>
    <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-12">
        <label for="">Titel</label>
        <input type="text" class="form-control" name="title" value="{{$aim->title}}" required="required">
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="3" required="required">{{$aim->description}}</textarea>
    </div>
    <div class="form-group col-sm-12 text-center">
        <label for="">Verteilung Nährstoffe in Prozent (muss insgesamt 100 ergeben)</label>
    </div>                    
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Fett</label>
        <input type="number" name="fat" class="form-control" min="0" max="100" value="{{$aim->fat}}" required="required">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kohlenhydrate</label>
        <input type="number" name="carbs" class="form-control" min="0" max="100" value="{{$aim->carbs}}" required="required">
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Proteine</label>
        <input type="number" name="protein" class="form-control" min="0" max="100" value="{{$aim->protein}}" required="required">
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@endsection
