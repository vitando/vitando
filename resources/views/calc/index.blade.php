@extends('calc.layout')

@section('layoutcontent')

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    Hallo {{ Auth::user()->name}}<br><br>

    Jetzt Reise <a href="/journey">beginnen!</a><br>
    <a href="/userarea/calendar">Kalender!</a><br>
    <a href="/userarea/pal">PAL-Wert berechnen!</a><br>
    <br><br>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    	<a href="/userarea/exercise">Deine Übungen</a> <br>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    	<a href="/exercise/index">Alle Übungen</a> <br>
    </div>
     
    

    
</div>

@endsection
