@extends('master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading text-center"><h4>Tools und Statistiken</h4></div>
                <div class="panel-body">
                @yield('layoutcontent')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
