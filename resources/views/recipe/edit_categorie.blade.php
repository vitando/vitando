@extends('recipe.layout')

@section('layoutcontent')

<!-- !!! Kategorien für Rezepte in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für Rezepte</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($recipes_categories as $orecipes_categorie)
        <tr>
            <td>{{$orecipes_categorie->title}} <a href="/recipecategorie/{{$orecipes_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $orecipes_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$orecipes_categorie->recipes_categories()->first()->title}}</td>
            @endif
            <td>{{$orecipes_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorien für Rezepte bearbeiten !!! -->

<form action="/recipecategorie/{{$recipes_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für Rezepte bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$recipes_categorie->title}}" placeholder="{{$recipes_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($recipes_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($recipes_categories as $arecipes_categorie)
                    <option value="{{$arecipes_categorie->id}}">{{$arecipes_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($recipes_categories as $arecipes_categorie)
                    @if($arecipes_categorie->id == $recipes_categorie->scope)
                        <option value="{{$arecipes_categorie->id}}" selected>{{$arecipes_categorie->title}}</option>
                    @else
                        <option value="{{$arecipes_categorie->id}}">{{$arecipes_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$recipes_categorie->description}}</textarea>
    </div>                    
    
    

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
