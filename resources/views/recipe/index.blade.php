@extends('recipe.layout')
@section('layoutcontent')
<!-- !!! Rezepte in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($recipes as $orecipe)
        <tr>
            <td> <a href="/recipe/{{$orecipe->id}}/show">{{$orecipe->title}}</a>  </td>
            <td>{{$orecipe->recipes_categories()->first()->title}}</td>
            <td>{{$orecipe->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
