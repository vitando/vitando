@extends('recipe.layout')

@section('layoutcontent')

<!-- !!! Rezepte anzeigen !!! -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h1>{{$recipe->title}}</h1>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h3>{{$recipe->description}}</h3>
    @if($check == false)
    <form action="/recipe/{{$recipe->id}}/add_to_user" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Favourite!</button>
    </div>
    </form>
    @endif
</div>

<!-- Charts -->
<input type="hidden" id="energy_kj" class="form-control" value="{{$recipe->energy_kj}}">
<input type="hidden" id="energy_kcal" class="form-control" value="{{$recipe->energy_kcal}}">
<input type="hidden" id="protein" class="form-control" value="{{$recipe->protein}}">
<input type="hidden" id="fat" class="form-control" value="{{$recipe->fat}}">
<input type="hidden" id="sugar" class="form-control" value="{{$recipe->sugar}}">
<input type="hidden" id="carbs" class="form-control" value="{{$recipe->carbs}}">

<canvas id="graph" width="500" height="200"></canvas>

<!-- Kommentarfunktion -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <legend>Kommentare</legend>

    @foreach($notes as $note)
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->value}}

        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->created_at}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->users->name}}
        </div>
        
    @endforeach

</div>

<form action="/recipe/{{$recipe->id}}/note" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Kommentar hinzufügen</legend>
    <div class="form-group">
        <label for="">Kommentar</label>
        <input type="text" class="form-control" name="note" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>



<!-- !!! Übungen in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Rezepte</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($recipes as $orecipe)
        <tr>
            <td><a href="/recipe/{{$orecipe->id}}/show">{{$orecipe->title}}</a></td>
            <td>{{$orecipe->recipes_categories()->first()->title}}</td>
            <td>{{$orecipe->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
