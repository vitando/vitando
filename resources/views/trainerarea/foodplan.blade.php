@extends('trainerarea.layout')

@section('layoutcontent')
<div class="row">
    <!-- !!! Kategorie für Ernährungsplan in Tabelle anzeigen !!! -->
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Kategorien für Ernährungspläne</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Hauptkategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodplans_categories as $foodplans_categorie)
                        <tr>
                            <td>{{$foodplans_categorie->title}} <a href="/foodplancategorie/{{$foodplans_categorie->id}}/edit" title="bearbeiten" class="glyphicon glyphicon-edit"></a></td>
                            @if( $foodplans_categorie->scope == 0)
                            <td>Das ist eine Hauptkategorie!</td>
                            @else
                            <td>{{$foodplans_categorie->foodplans_categories()->first()->title}}</td>
                            @endif
                            <td>{{ str_limit($foodplans_categorie->description, $limit = 100, $end = '...') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- !!! Ernährungspläne in Tabelle anzeigen !!! -->
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Ernährungspläne</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Kategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($foodplans as $foodplan)
                        <tr>
                            <td>{{$foodplan->title}} <a href="/foodplan/{{$foodplan->id}}/edit" class="glyphicon glyphicon-edit"></a><a href="/foodplan/{{$foodplan->id}}/delete" class="glyphicon glyphicon-remove"></a></td>
                            <td>{{$foodplan->foodplans_categories()->first()->title}}</td>
                            <td>{{ str_limit($foodplan->description, $limit = 25, $end = '...') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>   
    </div>
</div>

<!-- !!! Kategorie für Ernährungsplan erstellen !!! -->
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für Ernährungspläne erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/foodplancategorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required>
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($foodplans_categories as $foodplans_categorie)
                            <option value="{{$foodplans_categorie->id}}">{{$foodplans_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
            </div>                    

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>


<!-- !!! Ernährungspläne erstellen !!! -->

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Ernährungsplan erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/foodplan/create" method="POST" id="foodplan" role="form">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <div class="form-group col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <label for="">Titel</label>
                <input name="title" type="text" class="form-control" placeholder="">
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="">Kategorie</label>
                <select name="foodplans_categorie_id" class="form-control" required="required">
                    @foreach($foodplans_categories as $foodplans_categorie)
                            <option value="{{$foodplans_categorie->id}}">{{$foodplans_categorie->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="">Ziel</label>
                <select name="aim_id" class="form-control" required="required">
                    @foreach($aims as $aim)
                            <option value="{{$aim->id}}">{{$aim->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <label for="">Preis in €</label>
                <input type="number" name="price" class="form-control" value="5" min="5"} max="25" step="5" required="required" title="Der Verkaufsplan für den Träningplan">
            </div>
                
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required"></textarea>
            </div>

            <!-- Hier fängt der Tag an! -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
                <div class="row form-group">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                        <label for="">Name für Tag:</label>
                        <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" required>
                    </div>
                    <button type="button" title="Tag löschen" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
                    <button type="button" title="Tag kopieren" class="btn btn-default pull-left glyphicon glyphicon-plus add-plan-day"></button>
                </div>
                <div class="row form-group">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <label for="">Kategorie</label>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <label for="">Rezept</label>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <label title="" for="">Menge</label>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <label for="">Beschreibung / Notiz</label>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <label for=""><br></label>
                    </div>                                
                </div>
                <!--  Hier werden die Kategorien für die Rezepte dargestellt -->
                <div class="row form-group plan-insert">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <select id="recipe" class="form-control insert-categorie" required="required">
                            <option value="">Bitte auswählen!</option>
                            @foreach($recipes_categories as $recipes_categorie)
                                    <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!--  Hier werden die Rezepte dargestellt -->
                    <input name="recipes[]" type="hidden" class="form-control change-day-h" value="0">
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <select name="recipes[]" value="0" class="form-control insert-with-categorie" required="required"><option value="">Bitte auswählen!</option></select>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <input name="recipes[]" type="number" class="form-control" min="1" value="1">
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <input name="recipes[]" type="text" class="form-control" placeholder="">
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <button title="Rezept löschen" type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
                    </div>
                </div>
                <button type="button" class="btn btn-default glyphicon glyphicon-plus insert"></button> Rezept hinzufügen!
            </div>

            <button type="button" class="btn btn-success fplan-check">Ernährungsplan prüfen!</button>
            <button type="submit" class="btn btn-primary">Ernährungsplan speichern!</button>
        </form>

        <!-- Balken Diagramm für Ernährungsplan Prüfung -->
        <div id="fplan-check-result" class="hidden">
        </div>

        <!-- Button Übung modal -->
        <button type="button" class="btn btn-success btn-md pull-right" data-toggle="modal" data-target="#myModal">
          Neues Rezept anlegen! (ohne die Seite zu verlassen)
        </button>
        <!-- Modal Übung anlegen -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Rezept erstellen!</h4>
                    </div>
                        <form action="" method="POST" id="modal-recipe" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="modal-body">

                                <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <label for="">Titel</label>
                                    <input type="text" class="form-control" name="title" placeholder="" required="required">
                                </div>                    
                                <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <label for="">Kategorie</label>
                                    <select name="category" class="form-control" required="required">
                                        <option value="" selected>Bitte Auswählen</option>
                                        @foreach($recipes_categories as $recipes_categorie)
                                                <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="">Beschreibung</label>
                                    <textarea name="description" class="form-control" rows="5" required="required"></textarea>
                                </div> 
                                <div class="row text-center">
                                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                                        <label for="">Zutat</label>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <label for="">Menge</label>
                                    </div>
                                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                        <label for=""><br></label>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                                    <div class="row form-group ingredients">
                                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 magic-reset">
                                            <input type="text" class="form-control magic-food" name="food[]" required="required">    
                                        </div>
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                            <input type="number" class="form-control" name="food[]" placeholder="" min="1" required="required">  
                                        </div>
                                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                            <button type="button" class="btn btn-default pull-right glyphicon glyphicon-remove delete-div"></button>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-food"></button>
                                </div>
                                <div class="text-center">Eventuell musst Du die Kategorien wechseln um das Rezept neu zu laden!</div>               
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                
                                <button type="submit" class="btn btn-primary btn-modal-noReload-q" data-complete-text="gespeichert!">Rezept speichern!</button>
                            </div>
                        </form>   
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
