@extends('trainerarea.layout')
@section('layoutcontent')

<a class="btn btn-large btn-block btn-warning disabled" href="#" role="button">Du wurdest Eingeladen!</a><br>
<div class="row">
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3">
    @foreach($invites as $invite)
        <a class="btn btn-large btn-block btn-info disabled" href="#" role="button">{{$invite->shop->title}}</a><br>
    @endforeach
</div>
</div>
@if(!empty($shop))
    @if($shop->status == 'disable')
    <a class="btn btn-large btn-block btn-warning disabled" href="#" role="button">Shop wird noch geprüft!</a><br>
    @else
    <a class="btn btn-large btn-block btn-success disabled" href="#" role="button">{{$shop->title}}</a><br>
    <legend class="text-center">Trainer zum Shop einladen</legend>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3 text-center">
        <form action="/trainerarea/shop/invite" method="POST" class="form" role="form">
            {{ csrf_field() }}
            <div class="form-group">
                <input class="form-control magic-trainer" name="id">
            </div>
            <button type="submit" class="btn btn-primary">Einladen</button>
        </form>
    </div>
    @endif
@else
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Shop anlegen</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" method="POST" role="form" action="{{ url('/register/shop') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title" class="col-md-4 control-label">Shop Name</label>
                <div class="col-md-2">
                    <input name="title" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="firm" class="col-md-4 control-label">Firma</label>
                <div class="col-md-5">
                    <input name="firm" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="ust_id" class="col-md-4 control-label">Ust-Id.</label>
                <div class="col-md-2">
                    <input name="ust_id" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Adresse</label>
                <div class="col-md-5">
                    <input name="email" id="email" type="email" class="form-control" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="zip_code" class="col-md-4 control-label">PLZ</label>
                <div class="col-md-1">
                    <input name="zip_code" type="text" class="form-control" required>
                </div>
                <label for="town" class="col-md-1 control-label">Stadt</label>
                <div class="col-md-3">
                    <input name="town" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="street" class="col-md-4 control-label">Straße</label>
                <div class="col-md-3">
                    <input name="street" type="text" class="form-control" required>
                </div>
                <label for="street_number" class="col-md-1 control-label">Hausnummer</label>
                <div class="col-md-1">
                    <input name="street_number" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label for="street_add" class="col-md-4 control-label">Straßen Zusatz</label>
                <div class="col-md-5">
                    <input name="street_add" type="text" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-info">
                        Registrieren
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endif
@endsection
