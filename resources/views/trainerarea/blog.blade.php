@extends('trainerarea.layout')
@section('layoutcontent')
<div class="row">
    <!-- !!! Kategorien für Blogs in Tabelle anzeigen !!! -->
    <div class="col-md-6">
         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Kategorien für Blogs</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Hauptkategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blogs_categories as $blogs_categorie)
                        <tr>
                            <td>{{$blogs_categorie->title}} <a href="/blogcategorie/{{$blogs_categorie->id}}/edit" alt="bearbeiten" class="glyphicon glyphicon-edit"></a></td>
                            @if( $blogs_categorie->scope == 0)
                            <td>Das ist eine Hauptkategorie!</td>
                            @else
                            <td>{{$blogs_categorie->blogs_categories()->first()->title}}</td>
                            @endif
                            <td>{{$blogs_categorie->description}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- !!! Übungen in Tabelle anzeigen !!! -->
    <div class="col-md-6">
       <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Blogs</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Kategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blogs as $blog)
                        <tr>
                            <td>{{$blog->title}} <a href="/blog/{{$blog->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                            <td>{{$blog->blogs_categories()->first()->title}}</td>
                            <td>{{$blog->description}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
</div> 

<!-- !!! Kategorie für Blogs erstellen !!! -->

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für Blogs erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/blogcategorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required>
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($blogs_categories as $blogs_categorie)
                            <option value="{{$blogs_categorie->id}}">{{$blogs_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<!-- !!! Blog erstellen !!! -->
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Blog erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/blog/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required="required">
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Kategorie</label>
                <select name="category" class="form-control" required="required">
                    <option value="" selected>Bitte Auswählen</option>
                    @foreach($blogs_categories as $blogs_categorie)
                            <option value="{{$blogs_categorie->id}}">{{$blogs_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Kurzbeschreibung</label>
                <textarea name="description" class="form-control" rows="3" required="required"></textarea>
            </div>
            <div class="form-group col-sm-12">
                <label for="">Eintrag</label>
                <textarea name="text" class="form-control" rows="10" required="required"></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection
