@extends('trainerarea.layout')
@section('layoutcontent')
<div class="row">
    <!-- !!! Kategorien für users in Tabelle anzeigen !!! -->
    <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Kategorien für users</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Hauptkategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users_categories as $users_categorie)
                        <tr>
                            <td>{{$users_categorie->title}} <a href="/usercategorie/{{$users_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                            @if( $users_categorie->scope == 0)
                            <td>Das ist eine Hauptkategorie!</td>
                            @else
                            <td>{{$users_categorie->users_categories()->first()->title}}</td>
                            @endif
                            <td>{{$users_categorie->description}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- !!! User in Tabelle anzeigen !!! -->
    <div class="col-md-12">
       <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title text-center">User</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Ort</th>
                            <th>Geburtsdatum</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}} <a href="/user/{{$user->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                            <td>{{$user->addrs()->where('type', 'default')->first()->town}}</td>
                            <td>{{$user->addrs()->where('type', 'default')->first()->bday}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
</div> 

<!-- !!! Kategorie für users erstellen !!! -->
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für users erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/usercategorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required>
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($users_categories as $users_categorie)
                            <option value="{{$users_categorie->id}}">{{$users_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
