@extends('master')
@section('content')
<div class="container">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title text-center">Trainerarea</h3>
					</div>
					<div class="panel-body text-center">
					    <a href="/trainerarea/trainplan" class="col-xs-2 btn btn-primary" role="button">Trainingspläne</a>
					    <a href="/trainerarea/foodplan" class="col-xs-2 btn btn-primary" role="button">Ernährungspläne</a>
					    <a href="/trainerarea/exercise" class="col-xs-2 btn btn-primary" role="button">Übungen</a>
					    <a href="/trainerarea/recipe" class="col-xs-2 btn btn-primary" role="button">Rezepte</a>
					    <a href="/trainerarea/blog" class="col-xs-2 btn btn-primary" role="button">Blogs</a>
					    <a href="/trainerarea/aim" class="col-xs-2 btn btn-primary" role="button">Ziele</a>
					    <a href="/trainerarea/shop" class="col-xs-2 btn btn-primary" role="button">Shop</a>
					    <a href="/trainerarea/user" class="col-xs-2 btn btn-primary" role="button">User</a>
					</div>
				</div>

                @yield('layoutcontent')
</div>
@endsection
