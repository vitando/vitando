@extends('trainerarea.layout')

@section('layoutcontent')   
<!-- !!! Kategorie für Rezept in Tabelle anzeigen !!! -->
<div class="row">
    <div class="col-md-6">
         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Kategorien für Rezepte</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Hauptkategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($recipes_categories as $recipes_categorie)
                        <tr>
                            <td>{{$recipes_categorie->title}} <a href="/recipecategorie/{{$recipes_categorie->id}}/edit" alt="bearbeiten" class="glyphicon glyphicon-edit"></a></td>
                            @if( $recipes_categorie->scope == 0)
                            <td>Das ist eine Hauptkategorie!</td>
                            @else
                            <td>{{$recipes_categorie->recipes_categories()->first()->title}}</td>
                            @endif
                            <td>{{$recipes_categorie->description}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- !!! Rezepte in Tabelle anzeigen !!! -->
    <div class="col-md-6">
       <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Rezepte</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Kategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($recipes as $recipe)
                        <tr>
                            <td>{{$recipe->title}} <a href="/recipe/{{$recipe->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                            <td>{{$recipe->recipes_categories()->first()->title}}</td>
                            <td>{{$recipe->description}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
</div> 


<!-- !!! Kategorie für Rezept erstellen !!! -->

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für Rezepte erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/recipecategorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required="required">
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($recipes_categories as $recipes_categorie)
                            <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required"></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<!-- !!! Rezept erstellen !!! -->
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Rezept erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/recipe/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required="required">
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Kategorie</label>
                <select name="category" class="form-control" required="required">
                    <option value="" selected>Bitte Auswählen</option>
                    @foreach($recipes_categories as $recipes_categorie)
                            <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required"></textarea>
            </div> 
            <div class="row text-center">
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                    <label for="">Zutat</label>
                </div>

                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="">Menge</label>
                </div>

                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                <div class="row form-group ingredients">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 magic-reset">
                        <input type="text" class="form-control magic-food" name="food[]" required="required">    
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <input type="number" class="form-control" name="food[]" placeholder="" min="1" required="required">  
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                        <button type="button" class="btn btn-default pull-right glyphicon glyphicon-remove delete-div"></button>
                    </div>
                </div>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-food"></button>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@endsection
