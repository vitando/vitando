@extends('trainerarea.layout')

@section('layoutcontent')
<div class="row">
    <!-- !!! Kategorien für Übungen in Tabelle anzeigen !!! -->
    <div class="col-md-6">
         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Kategorien für Übungen</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Hauptkategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($exercises_categories as $exercises_categorie)
                        <tr>
                            <td>{{$exercises_categorie->title}} <a href="/exercisecategorie/{{$exercises_categorie->id}}/edit" alt="bearbeiten" class="glyphicon glyphicon-edit"></a></td>
                            @if( $exercises_categorie->scope == 0)
                            <td>Das ist eine Hauptkategorie!</td>
                            @else
                            <td>{{$exercises_categorie->exercises_categories()->first()->title}}</td>
                            @endif
                            <td>{{$exercises_categorie->description}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- !!! Übungen in Tabelle anzeigen !!! -->
    <div class="col-md-6">
       <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Übungen</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Titel</th>
                            <th>Kategorie</th>
                            <th>Beschreibung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($exercises as $exercise)
                        <tr>
                            <td>{{$exercise->title}} <a href="/exercise/{{$exercise->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                            <td>{{$exercise->exercises_categories()->first()->title}}</td>
                            <td>{{$exercise->description}}</td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div> 
    </div> 
</div> 

<!-- !!! Kategorie für Übungen erstellen !!! -->

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Kategorie für Übungen erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/exercisecategorie/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required>
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Hauptkategorie</label>
                <select name="scope" class="form-control" required="required">
                    <option value="0" selected>Ist eine Hauptkategorie</option>
                    @foreach($exercises_categories as $exercises_categorie)
                            <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required" required></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

<!-- !!! Übung erstellen !!! -->
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Übung erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/exercise/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required="required">
            </div>                    
            <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Kategorie</label>
                <select name="category" class="form-control" required="required">
                    <option value="" selected>Bitte Auswählen</option>
                    @foreach($exercises_categories as $exercises_categorie)
                            <option value="{{$exercises_categorie->id}}">{{$exercises_categorie->title}}</option>
                    @endforeach
                </select>
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="5" required="required"></textarea>
            </div>                    
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
