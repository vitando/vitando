@extends('trainerarea.layout')
@section('layoutcontent')

<!-- !!! Ziele in Tabelle anzeigen !!! -->
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Ziele</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Titel</th>
                    <th>Beschreibung</th>
                    <th>Fett</th>
                    <th>Kohlenhydrate</th>
                    <th>Proteine</th>
                </tr>
            </thead>
            <tbody>
                @foreach($aims as $aim)
                    <tr>
                        <td>{{$aim->title}} <a href="/trainerarea/aim/{{$aim->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
                        <td>{{$aim->description}}</td>
                        <td>{{$aim->fat}}</td>
                        <td>{{$aim->carbs}}</td>
                        <td>{{$aim->protein}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<!-- !!! Ziel erstellen !!! -->

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Ziel erstellen</h3>
    </div>
    <div class="panel-body">
        <form action="/trainerarea/aim/create" method="POST" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-12">
                <label for="">Titel</label>
                <input type="text" class="form-control" name="title" placeholder="" required="required">
            </div>                    
            <div class="form-group col-sm-12">
                <label for="">Beschreibung</label>
                <textarea name="description" class="form-control" rows="3" required="required"></textarea>
            </div>
            <div class="form-group col-sm-12 text-center">
                <label for="">Verteilung Nährstoffe in Prozent (muss insgesamt 100 ergeben)</label>
            </div>                    
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Fett</label>
                <input type="number" name="fat" class="form-control" min="0" max="100" required="required">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Kohlenhydrate</label>
                <input type="number" name="carbs" class="form-control" min="0" max="100" required="required">
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <label for="">Proteine</label>
                <input type="number" name="protein" class="form-control" min="0" max="100" required="required">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection
