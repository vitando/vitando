@extends('foodplan.layout')

@section('layoutcontent')

<!-- !!! Kategorien für Ernährungspläne in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Kategorien für Ernährungspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Hauptkategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($foodplans_categories as $ofoodplans_categorie)
        <tr>
            <td>{{$ofoodplans_categorie->title}} <a href="/foodplancategorie/{{$ofoodplans_categorie->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            @if( $ofoodplans_categorie->scope == 0)
            <td>Das ist eine Hauptkategorie!</td>
            @else
            <td>{{$ofoodplans_categorie->foodplans_categories()->first()->title}}</td>
            @endif
            <td>{{$ofoodplans_categorie->description}}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>


<!-- !!! Kategorie für Übung bearbeiten !!! -->

<form action="/foodplancategorie/{{$foodplans_categorie->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Kategorie für Ernährungspläne bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$foodplans_categorie->title}}" placeholder="{{$foodplans_categorie->title}}" required="required">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Hauptkategorie</label>
        <select name="scope" class="form-control" required="required">

            @if($foodplans_categorie->scope == 0)
                <option value="0" selected>Das ist eine Haupkategorie!</option>

                @foreach($foodplans_categories as $afoodplans_categorie)
                    <option value="{{$afoodplans_categorie->id}}">{{$afoodplans_categorie->title}}</option>
                @endforeach
            @else
                <option value="0">Das ist eine Haupkategorie!</option>
                
                @foreach($foodplans_categories as $afoodplans_categorie)
                    @if($afoodplans_categorie->id == $foodplans_categorie->scope)
                        <option value="{{$afoodplans_categorie->id}}" selected>{{$afoodplans_categorie->title}}</option>
                    @else
                        <option value="{{$afoodplans_categorie->id}}">{{$afoodplans_categorie->title}}</option>
                    @endif
                @endforeach
            @endif

        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$foodplans_categorie->description}}</textarea>
    </div>                    
    
    

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
