@extends('foodplan.layout')
@section('layoutcontent')

<!-- !!! Ernährungspläne anzeigen !!! -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h1>{{$foodplan->title}}</h1>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
    <h3>{{$foodplan->description}}</h3>
    @if($check == false)
    <form action="/foodplan/{{$foodplan->id}}/add_to_user" method="POST" role="form">
    <div class="form-group">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Favourite!</button>
    </div>
    </form>
    @endif
</div>

<!-- Kommentarfunktion -->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <legend>Kommentare</legend>
    @foreach($notes as $note)
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->value}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->created_at}}
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            {{$note->users->name}}
        </div>
    @endforeach
</div>

<form action="/foodplan/{{$foodplan->id}}/note" method="POST" role="form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend>Kommentar hinzufügen</legend>
    <div class="form-group">
        <label for="">Kommentar</label>
        <input type="text" class="form-control" name="note" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<!-- !!! Übungen in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Ernährungspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($foodplans as $ofoodplan)
        <tr>
            <td><a href="/foodplan/{{$ofoodplan->id}}/show">{{$ofoodplan->title}}</a></td>
            <td>{{$ofoodplan->foodplans_categories()->first()->title}}</td>
            <td>{{ str_limit($ofoodplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
