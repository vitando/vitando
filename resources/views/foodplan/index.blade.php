@extends('foodplan.layout')
@section('layoutcontent')
<!-- !!! Ernährungspläne in Tabelle anzeigen !!! -->
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($foodplans as $ofoodplan)
        <tr>
            <td> <a href="/foodplan/{{$ofoodplan->id}}/show">{{$ofoodplan->title}}</a>  </td>
            <td>{{$ofoodplan->foodplans_categories()->first()->title}}</td>
            <td>{{ str_limit($ofoodplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

@endsection
