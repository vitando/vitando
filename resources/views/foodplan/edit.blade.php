@extends('foodplan.layout')
@php ($old_day = $foodplan->foodplans_recipes()->first()->day)
@php ($foodplans_recipes = $foodplan->foodplans_recipes()->get())
@php ($foodplans_recipes = $foodplans_recipes->sortBy('day'))

@section('layoutcontent')

<!-- !!! Ernährungspläne in Tabelle anzeigen !!! -->

<table class="table table-striped table-hover">
    <legend class="text-center">Deine Ernährungspläne</legend>
    <thead>
        <tr>
            <th>Titel</th>
            <th>Kategorie</th>
            <th>Beschreibung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($foodplans as $ofoodplan)
        <tr>
            <td>{{$ofoodplan->title}} <a href="/foodplan/{{$ofoodplan->id}}/edit" class="glyphicon glyphicon-edit"></a></td>
            <td>{{$ofoodplan->foodplans_categories()->first()->title}}</td>
            <td>{{ str_limit($ofoodplan->description, $limit = 100, $end = '...') }}</td>
        </tr>
            
        @endforeach

    </tbody>
</table>

<!-- !!! Ernährungspläne bearbeiten !!! -->

<form action="/foodplan/{{$foodplan->id}}" method="POST" role="form">
    {{method_field('PATCH') }}
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <legend class="text-center">Ernährungsplan bearbeiten</legend>


    <div class="form-group col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <label for="">Titel</label>
        <input name="title" type="text" class="form-control" value="{{$foodplan->title}}" placeholder="{{$foodplan->title}}">
    </div>                    
    <div class="form-group col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <label for="">Kategorie</label>
        <select name="foodplans_categorie_id" class="form-control" required="required">
            <option value="{{$foodplan->foodplans_categorie_id}}" selected>{{$foodplan->foodplans_categories()->first()->title}}</option>
            @foreach($foodplans_categories as $foodplans_categorie)
                    <option value="{{$foodplans_categorie->id}}">{{$foodplans_categorie->title}}</option>
            @endforeach
        </select>
    </div>                    
    <div class="form-group col-sm-12">
        <label for="">Beschreibung</label>
        <textarea name="description" class="form-control" rows="5" required="required">{{$foodplan->description}}</textarea>
    </div>                    
    
    <!-- Hier fängt der Tag an! -->

        
    @foreach($foodplans_recipes as $recipe)
        @if($loop->first)
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$recipe->day}}" required>
                </div>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-div" disabled></button>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus add-plan-day"></button>
            </div>
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label for="">Rezept</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Menge</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
        @endif
        @if($recipe->day != $old_day)
            <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus insert"></button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center plan-day">
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-sm-offset-4">
                    <input type="text" title="Gib hier den Namen für deinen definierten Tag ein" name="day_categorie" class="form-control change-day" value="{{$recipe->day}}" required>
                </div>
                <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-plan-day" disabled></button>
            </div>
            <div class="row form-group">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Kategorie</label>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <label for="">Rezept</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Menge</label>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <label for="">Beschreibung / Notiz</label>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <label for=""><br></label>
                </div>                                
            </div>
            <div class="row form-group plan-insert">
            <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="recipe" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($recipes_categories as $recipes_categorie)
                                <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="recipes[]" type="hidden" class="form-control new-day-id" value="{{$recipe->id}}">
                <input name="recipes[]" type="hidden" class="form-control change-day-h" value="{{$recipe->day}}">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select name="recipes[]" class="form-control insert-with-categorie" required="required" readonly><option value="{{ $recipe->recipe()->first()->id }}">{{ $recipe->recipe()->first()->title }}</option></select>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="number" class="form-control" value="{{ $recipe->amount }}">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="text" class="form-control" value="{{ $recipe->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert" disabled></button>
                </div>
            </div>
            @php ($old_day = $recipe->day)
        @else        
            <div class="row form-group plan-insert">
                <!--  Hier werden die Kategorien für die Übungen dargestellt -->
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <select id="recipe" class="form-control insert-categorie">
                        <option value="">Bitte auswählen!</option>
                        @foreach($recipes_categories as $recipes_categorie)
                                <option value="{{$recipes_categorie->id}}">{{$recipes_categorie->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!--  Hier werden die Übungen dargestellt -->
                <input name="recipes[]" type="hidden" class="form-control new-day-id" value="{{$recipe->id}}">
                <input name="recipes[]" type="hidden" class="form-control change-day-h" value="{{$recipe->day}}">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select name="recipes[]" class="form-control insert-with-categorie" required="required" readonly><option value="{{ $recipe->recipe()->first()->id }}">{{ $recipe->recipe()->first()->title }}</option></select>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="number" class="form-control" value="{{ $recipe->amount }}">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <input name="recipes[]" type="text" class="form-control" value="{{ $recipe->description }}">
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <button type="button" class="btn btn-default pull-left glyphicon glyphicon-remove delete-insert"></button>
                </div>
            </div>
            @php ($old_day = $recipe->day)
        @endif
    @endforeach

        <button type="button" class="btn btn-default pull-left glyphicon glyphicon-plus insert"></button>
    </div> 
    <button type="submit" class="btn btn-primary">Submit</button>


</form>


@endsection
