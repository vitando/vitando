@extends('order.layout')
@section('layoutcontent')

<!-- !!! Ziele in Tabelle anzeigen !!! -->
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title text-center">Warenkorb</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Titel</th>
                    <th>Preis</th>
                    <th>Menge</th>
                    <th>Gesamt</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<button type="button" class="btn btn-success pull-right">Zur Kasse</button>
@endsection
