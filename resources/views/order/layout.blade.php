@extends('master')

@section('content')
<div class="container">
    @yield('layoutcontent')
</div>
@endsection
