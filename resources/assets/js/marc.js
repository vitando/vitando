
//  ------------------------ Marc*s Spezial ------------------

$(document).on('click', '.clone', function() {
    var target = $(this).data('target');
    // $(target).clone(); // pseudoode
    var cloned = $(target).last().clone();
    cloned.find('input, select, textarea').each(function()
    {
        $(this).val('');
    });

    afterClone($(this));


    // <div class="exercices">
    //  <div class="trainplan-exercise">/**/</div>
    //  <div class="trainplan-exercise">/**/</div>
    // </div>

    // <button class="clone" data-target=".trainplan-exercise"></button>


    // <div class="recipes">
    //  <div class="recipe">/**/</div>
    //  <div class="recipe">/**/</div>
    // </div>

    // <button class="clone" data-target=".recipe"></button>
});

function afterClone(element)
{
    if(element.data('target') == 'recipe')
    {
        // code ausführen
    }
    else if(element.data('target') == 'trainplan-exercise')
    {
        // anderen code ausführen
    }
}