$(function() {
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
  });
});
$(document).on('change', '.change-day', function(){
        var day = $(this).val();
        $(this).parent().parent().parent().find('.change-day-h').val(day);
    });
$(document).on('click', '.delete-div', function(){
        $(this).parent().parent().remove();
    });
$(document).on('click', '.delete-event', function(){
        $(this).parent().find('.event-post').attr("data-type", "delete");
        $(this).parent().hide();
    });
$(document).on('click', '.delete-insert', function(){
        $(this).parent().parent().hide().find('.change-day-h').attr('type', 'hidden').val('delete');
    });
$(document).on('click', '.insert', function(){
        var insert = $(this).parent().find(".plan-insert").last().clone();
        $(this).parent().find(".plan-insert").last().after(insert);
        $(this).parent().find(".plan-insert").last().show().find('.delete-div').attr("disabled", false);
        $(this).parent().find(".plan-insert").last().find('.insert-with-categorie').last().empty().attr("readOnly", true);
        $(this).parent().find(".plan-insert").last().find('.new-day-id').val('new');
    });

$(document).on('change', '.insert-categorie', function(){
    var type = $(this).attr("id");
    var categorie_id = $(this).val();
    var items = '';

    $.ajax({
      url: '/gym/' + type + '/by_categorie',
      data: {categorie_id: categorie_id},
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items += '<option value=' + data.id + '>' + data.title + '</option>';
        });
      }
    });
    $(this).parent().parent().find(".insert-with-categorie").empty().append(items).attr("readOnly", false);
});

$(document).on('focusout', '.pal-hour', function(){
    var clone = $(this).parent().parent().parent().clone();
    var hour = 0;
    $(this).parent().parent().parent().parent().find(".pal-hour").each(function(){
        hour = hour + +($(this).val());
        console.log(hour);
    });
    var left = 24 - hour;
    if (left > 0) {
        insert = $(this).parent().parent().parent().parent().find(".pal").last().after(clone);
        $(this).parent().parent().parent().parent().find(".pal-hour").last().val(left);
    }else if(left < 0){
        last = $(this).parent().parent().parent().parent().find(".pal-hour").last().val();
        left = +(last) + +(left);
        $(this).parent().parent().parent().parent().find(".pal-hour").last().val(left);
    }
});

$(document).on('click', '.compareFPlan', function(){
    var stats = $('input[name="pages_title[]"]').val();;
    var items = '';

    $.ajax({
      url: '/gym/' + type + '/by_categorie',
      data: {categorie_id: categorie_id},
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items += '<option value=' + data.id + '>' + data.title + '</option>';
        });
      }
    });
    $(this).parent().parent().find(".insert-with-categorie").empty().append(items).prop("disabled", false);
});

$(document).on('click', '.add-food', function(){
        var food = $(this).parent().find(".ingredients").last().clone();
        $(this).parent().find(".ingredients").last().after(food);
        $(this).parent().find(".ingredients").last().show().find('.delete-div').prop("disabled", false);
        $(this).parent().find(".ingredients").last().show().find('.change-day-h').attr('type', 'number').attr('value', '1');
        $(this).parent().find(".ingredients").last().find('.new-day-id').val('new');
        $(this).parent().find(".ingredients").last().find('.magic-reset').empty().append('<input type="text" class="form-control" name="food[]">').find('input').magicSuggest({
        method: 'get',
        data: '/food/find',
        valueField: 'id',
        displayField: 'name',
        allowFreeEntries: true,
        maxSelection: 1,
        maxSuggestions: 10,
        hideTrigger: true,
        matchCase: true
    });
});

$(document).on('click', '.fplan-check', function(){
    $.get( "/trainerarea/foodplan/check", $("#foodplan").serialize(), function( data ) {
        var data = {
            labels: ['protein', 'fat', 'carbs'],
            datasets: [
                {   
                    label: 'Ziel',
                    borderColor: 'rgba(0, 0, 0, 0)',
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.7)",
                        "rgba(75, 192, 192, 0.7)",
                        "rgba(255, 206, 86, 0.7)",
                    ],
                    data: [data.aim_protein, data.aim_fat, data.aim_carbs],
                },
                {   
                    label: 'Aktuell',
                    borderColor: 'rgba(0, 0, 0, 0)',
                    backgroundColor: [
                        "rgba(255, 99, 132, 0.5)",
                        "rgba(75, 192, 192, 0.5)",
                        "rgba(255, 206, 86, 0.5)",
                    ],
                    data: [data.result_protein, data.result_fat, data.result_carbs],
                }
            ]
        }

        $('#fplan-check-result').empty().append('<canvas id="fplan-result" width="400" height="200"></canvas>');
        var context = document.querySelector('#fplan-result').getContext('2d');
        var myNewChart = new Chart(context , {
            type: 'bar',
            data: data
        }); 
        $('#fplan-check-result').removeClass('hidden');

    });
});

$(document).on('click', '.add-plan-day', function(){
    var trainplanDay = $(this).parent().parent().clone();
    $(this).parent().parent().parent().find('.plan-day').last().after(trainplanDay);
    $(this).parent().find('.delete-div').prop("disabled", false);
    $(this).parent().find('.add-plan-day').prop("disabled", true);
    $(this).parent().parent().parent().find('.plan-day').last().find('.new-day-id').val('new');
});

$(document).on('click', '.save-clndr', function(){
    var days = $(document).find('.droppable');
    var items = new Array();
    var i = 0;
    $.each(days, function(id, data){
        var day = $(data).find('.draggable');
        var event_date = $(data).attr('id');
        $.each(day, function(id, data){
            var event_id = $(data).data("id");
            var event_type = $(data).data("type");
            var event_name = $(data).data("name");
            items[i] = {event: {event_date: event_date, event_id: event_id, event_type: event_type, event_name: event_name}};
            i++;
        });
    });
    var data = JSON.stringify(items);
    $.ajax({
      url: '/userarea/events/create',
      async: false,
      dataType: 'json',
      data: {data: data}
    });
    location.reload();
});

$(document).on('click', '.update-clndr', function(){
    var days = $(document).find('.event-item');
    var items = new Array();
    var i = 0;
    $.each(days, function(id, data){
        var day = $(data).find('.event-post');
        $.each(day, function(id, data){
            var data_id = $(data).attr("data-event-id");
            var event_date = $(data).data("date");
            var event_type = $(data).data("type");
            items[i] = {event: {event_date: event_date, data_id: data_id, event_type: event_type}};
            i++;
        });
    });
    var data = JSON.stringify(items);
    $.ajax({
      url: '/userarea/events/update',
      async: false,
      dataType: 'json',
      data: {data: data}
    });
    location.reload();
});

$(document).on('click', '.get-fprecipe', function(){
    var id_plan = $(this).data("id");
    var name = $(this).data("name");
    var items = '';
    $.ajax({
      url: '/foodplan/' + id_plan + '/day/' + name,
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items += '<tr><td><span data-id="' + data.id + '" data-type="recipe" data-name="' + data.title + '" class="label label-success draggable">' + data.title + '</span></td>'+
                '<td>' + data.amount + '</td>'+
                '<td>' + data.protein + '</td>'+
                '<td>' + data.carbs + '</td>'+
                '<td>' + data.fat + '</td>'+
              '</tr>';
        });
      }
    });
    $(document).find('.insert-fpday').empty().append(items);
    makedrag();
});

$(document).on('click', '.get-tpexercise', function(){
    var id_plan = $(this).data("id");
    var name = $(this).data("name");
    var items = '';

    $.ajax({
      url: '/trainplan/' + id_plan + '/day/' + name,
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items += '<tr>'+'<td><span data-id="' + data.id + '" data-type="exercise" data-name="' + data.title + '" class="label label-success draggable">' + data.title + '</span></td>'+
                '<td>' + data.reps + '</td>'+
                '<td>' + data.sets + '</td>'+
                '<td>' + data.weight + '</td>'+
              '</tr>';
        });
      }
    });
    $(document).find('.insert-tpday').empty().append(items);
    makedrag();
});

$(function() {
    $( ".magic-food" ).each(function(){
        if ($(this).val().length){
            var id = $(this).val();    
            $(this).parent().empty().append('<input type="text" class="form-control" name="food[]">').find('input').magicSuggest({
                method: 'get',
                data: '/food/find',
                value: [id],
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
        else{
            $('.magic-food').magicSuggest({
                method: 'get',
                data: '/food/find',
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
    });
});

$(function() {
    $( ".magic-trainer" ).each(function(){
        if ($(this).val().length){
            var id = $(this).val();    
            $(this).parent().empty().append('<input type="text" class="form-control" name="trainer[]">').find('input').magicSuggest({
                method: 'get',
                data: '/trainer/find',
                value: [id],
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
        else{
            $('.magic-trainer').magicSuggest({
                method: 'get',
                data: '/trainer/find',
                valueField: 'id',
                displayField: 'name',
                allowFreeEntries: true,
                maxSelection: 1,
                maxSuggestions: 10,
                hideTrigger: true,
                matchCase: true
            });
        }
    });
});

$(document).on('click', '.btn-modal-noReload', function(event){
    event.preventDefault();
    var $btn = $(this);
    $btn.button('loading');
    $.ajax({
      url: '/exercise/create',
      data: $("#modal-exercise").serialize(),
      async: false,
      method: 'POST',
      success: function (data) {
        $btn.button('complete').prop("disabled", true);
      }
    });
});

$(document).on('click', '.btn-modal-noReload-q', function(event){
    event.preventDefault();
    var $btn = $(this);
    $btn.button('loading');
    $.ajax({
      url: '/recipe/create',
      data: $("#modal-recipe").serialize(),
      async: false,
      method: 'POST',
      success: function (data) {
        $btn.button('complete').prop("disabled", true);
      }
    });
});

//  --- Step by Step Modal definition
$(document).ready(function(){
    $('#myModal').modalSteps({
        btnCancelHtml: 'Abbrechen',
        btnPreviousHtml: 'Zurück',
        btnNextHtml: 'Weiter',
        btnLastStepHtml: 'Abschließen!',
        disableNextButton: false,
        callbacks: {}
    });
});

$('.journey').submit(function(event){
    event.preventDefault();
    $url = $(this).attr('action');
    $.ajax({
      url: $url,
      data: $(this).serialize(),
      async: false,
      method: 'POST',
      success: function () {
        $('#journey').modal({backdrop: 'static', keyboard: false});
        $('#journey').modal('show');
      }
    });
});

$(document).ready(function(){
    $('#journey').modalSteps({
        btnCancelHtml: 'Zurück zur Seite',
        btnPreviousHtml: 'Zurück',
        btnNextHtml: 'Weiter',
        btnLastStepHtml: 'Abschließen!',
        disableNextButton: false,
        completeCallback: function(){
            $.ajax({
                  url: '/userarea/journey',
                  data: $('#journey-modal').serialize(),
                  async: false,
                  method: 'POST',
                  success: function () {
                    window.location.href = "/";
                  }
                });
        },
        callbacks: {}
    });
});
$("#clndrModal").on("show.bs.modal", function(event) {
    var link = $(event.relatedTarget);
    $(this).find(".modal-content").load(link.attr("href"));
});
function makedrag() {
    $(".draggable").draggable({helper: "clone",cursor: 'move'});
}
function makedrop() {
    $('.droppable').droppable({
        drop: function (event, ui) {
            var $canvas = $(this);
            if (!ui.draggable.hasClass('canvas-element')) {
                var $canvasElement = ui.draggable.clone();
                $canvasElement.addClass('canvas-element');
                $canvasElement.draggable({
                    containment: '.droppable'
                });
                $canvas.append($canvasElement);
                $canvasElement.css({
                    // left: (ui.position.left),
                    // position: 'relative'
                });
                $canvasElement.draggable( 'disable' );
                $canvasElement.wrap('<span><span></span></span>');
                $canvasElement.after('<span class="btn btn-danger delete-div glyphicon glyphicon-remove"></span>');
            }
        }
    });
}

$(document).ready(function(){
    makedrag();
    makedrop();
});
