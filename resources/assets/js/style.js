$(document).ready(function(){
    if ($('#energy_kj').val()) {
        var energy_kj = $('#energy_kj').val();
        var energy_kcal = $('#energy_kcal').val();
        var protein = $('#protein').val();
        var fat = $('#fat').val();
        var sugar = $('#sugar').val();
        var carbs = $('#carbs').val();

        var data = {
            labels: ['protein', 'fat', 'sugar', 'carbs'],

            datasets: [
            {   
                label: 'Energie',
                borderColor: 'rgba(0, 0, 0, 0)',
                backgroundColor: [
                    "rgba(255, 99, 132, 0.6)",
                    "rgba(75, 192, 192, 0.6)",
                    "rgba(255, 206, 86, 0.6)",
                    "rgba(54, 162, 235, 0.6)",
                ],
                data: [protein, fat, sugar, carbs],
            }
            ]
        }
        var context = document.querySelector('#graph').getContext('2d');

        var myNewChart = new Chart(context , {
            type: 'bar',
            data: data
        }); 
    }
});

function userevents() {
    var items = new Array();
    $.ajax({
      url: '/userarea/events/get',
      async: false,
      dataType: 'json',
      success: function (data) {
        $.each(data, function(id, data) {
            items[id] = {date: data.event_date, id: data.event_id, eventID: data.id, type: data.event_type, title: data.event_name};
        });
      }
    });
    return items;
}

$('#cal1').clndr({
    template: $('#swag').html(),
    startWithMonth: moment(),
    weekOffset: 1,
    events: userevents(),
    daysOfTheWeek: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    clickEvents: {
        click: function(target) {
            console.log(target);
        },
        onMonthChange: function(month) {
          console.log('you just went to ' + month.format('MMMM, YYYY'));
          makedrag();
          makedrop();
        }
        },
        doneRendering: function() {
        console.log();
      },
});

